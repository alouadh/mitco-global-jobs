﻿using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Threading;

namespace Scheduled.Task.Manager.Models.BJscrapper
{
    class BJscrapper
    {

        private SGBD sg = new SGBD(ConfigurationManager.AppSettings["BJdb"]);
        public void getCharges()
        {

            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJScrapper/BJScrapperlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();


            InternetExplorerDriverService service = InternetExplorerDriverService.CreateDefaultService();
            service.SuppressInitialDiagnosticInformation = true;
            InternetExplorerDriver driver = new InternetExplorerDriver(service);

            try
            {
                string BJloginURL = ConfigurationManager.AppSettings["BJloginURL"];
                string BJuserID = ConfigurationManager.AppSettings["BJuserID"];
                string BJpassword = ConfigurationManager.AppSettings["BJpassword"];
                string BJscrappURL = ConfigurationManager.AppSettings["BJscrappURL"].ToString();

                driver.Navigate().GoToUrl(BJloginURL);
                
                Thread.Sleep(3000);

                driver.FindElement(By.Id("userID")).SendKeys(BJuserID);
                driver.FindElement(By.Id("password")).SendKeys(BJpassword);

                driver.FindElement(By.Id("userSubmit")).Click();

                Thread.Sleep(3000);



                string sql = @" select
      r.recvid recvid,
      r.custrefno2 shipmentid
  from 
  mitcoltd.dbo.receive r,BlujayEDI.dbo.blujay_message b
  where r.customerid = 'MEIMI'
  and b.ShipmentID = rtrim(r.custrefno2)
  and round((select top 1 convert(decimal(12,2),isnull(replace(bb.charge,',',''),0)) from blujayedi.dbo.blujay_message bb where bb.ShipmentID = r.custrefno2 order by id desc)/100,0)
<>
round(IsNull(r.rbillrate, 0)+IsNull(r.rFuelSur, 0)+IsNull(r.rAccAmount, 0)+IsNull(r.rAccAmount1, 0)+IsNull(r.rAccAmount2, 0),0)
  and  convert(datetime,r.om_order_date) >= getdate()-90
  and  r.om_order_date is not null
  and r.om_status = 'Closed'
  and isnull(r.cancelflag,'') <> '1'
  and b.Id = (select top 1 bm.Id from BlujayEDI.dbo.blujay_message bm where bm.ShipmentID = b.ShipmentID order by bm.Id desc)";

                DataTable dt = sg.getTableByCommande(sql);

                for (int i = 0; i < dt.Rows.Count - 1; i++)
                {
                    try
                    {
                        driver.Navigate().GoToUrl(BJscrappURL + dt.Rows[i][1].ToString());
                        string bjCharges = driver.FindElement(By.Id("rateVal" + dt.Rows[i][1].ToString())).Text.Replace(" USD", "").Replace(",","");
                        string bjCharges2 = bjCharges.Split('.')[0];
                        try
                        {
                            bjCharges2 += int.Parse(bjCharges.Split('.')[1]).ToString("00");
                        }
                        catch (Exception)
                        {
                        }
                        BJlog.Information("BJ ID : " + dt.Rows[i][1].ToString() + "- getCharges : " + bjCharges2);
                        sg.Execute("update blujayedi.dbo.blujay_message set Charge = '" + bjCharges2 + "' where ShipmentID = '" + dt.Rows[i][1].ToString() + "'");
                    }
                    catch (Exception ex)
                    {
                        BJlog.Error("getCharges1 : " + ex.Message);
                    }
                   
                }
            }
            catch (Exception ex)
            {
                BJlog.Error("getCharges : " + ex.Message);
            }
            finally
            {
                driver.Quit();
            }

        }


    }
}
