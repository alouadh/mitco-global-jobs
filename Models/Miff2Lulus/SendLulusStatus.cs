﻿using RestSharp;
using Serilog;
using Serilog.Sinks.LiteDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace Scheduled.Task.Manager.Models.Miff2Lulus
{
    class SendLulusStatus
    {
        public class LulusResponse
        {
            public string apiVersion { get; set; }
            public string context { get; set; }
            public Data data { get; set; }
        }

        public class Data
        {
            public string number { get; set; }
            public string status { get; set; }
            // public string status_date { get; set; }
            public List<Item> items { get; set; }
        }

        public class Item
        {
            public string productId { get; set; }
            public string skuId { get; set; }
            public string upc { get; set; }
            public Qty qty { get; set; }
            //public Qty overqty { get; set; }
            //public Qty shortqty { get; set; }
            //public Qty damagedqty { get; set; }
            //public Qty quarantineqty { get; set; }
            //public Qty shippedqty { get; set; }
        }

        public class Qty
        {
            public decimal Chico { get; set; }
            public decimal Penn { get; set; }
            public decimal Vc { get; set; }
        }


        public class QReslt
        {
            public string pono { get; set; }
            public string ProductID { get; set; }
            public string sku { get; set; }
            public string upc { get; set; }
            public decimal packqty { get; set; }
            public decimal overqty { get; set; }
            public decimal shortqty { get; set; }
            public decimal damagedqty { get; set; }
            public decimal quarantineqty { get; set; }
            public decimal qtyshipped { get; set; }
            public string location { get; set; }
            public string status_date { get; set; }
            public string status { get; set; }
        }


        public int sendStatusGlobal(string args, string endpoint, string auth, string cnx)
        {
            // var LUlog = new LoggerConfiguration().WriteTo.LiteDB("logs/LULUS/LULUSlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var LUlog = new LoggerConfiguration().WriteTo.File("logs/LULUS/LULUSlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();

            try
            {
                SGBD sg = new SGBD(cnx);

                ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;


                List<LulusResponse> response_list = new List<LulusResponse>();
                string query_header = " select " +
                                        " r.pono, " +
                                        " r.ProductID, " +
                                        " r.sku, " +
                                        " r.upc, " +
                                        " convert(decimal(18,0), r.packqty + isnull((select top 1 Qty from lulus_studio_pull p where p.recvitemid = r.recvitemid and p.origin_qty <> '-'),0)) packqty, " +
                                        " convert(decimal(18,0), r.overqty) overqty, " +
                                        " convert(decimal(18,0), r.shortqty) shortqty, " +
                                        " convert(decimal(18,0), r.damagedqty) damagedqty, " +
                                        " convert(decimal(18,0), r.quarantineqty) quarantineqty, " +
                                        " convert(decimal(18,0), r.qtyshipped) qtyshipped, " +
                                        " (select top 1 name from api_location_translate where translation = r.location) location, " +
                                        " convert(varchar(10),@@status_date@@,21) status_date," +
                                        " @@status@@ status " +
                                        " from " +
                                        " recvitems r, receive rr " +
                                        " where r.userid=989 " +
                                        " and r.recvid = rr.recvid " +
                                        " and rr.customerid = 'LUCH001' " +
                                        " AND (select count(a.po) from api_importation_lulus_status a where a.STATUS = @@status@@ and a.po = rtrim(r.pono)) = 0 ";

                //string query_to_receive = " and rr.received is null and rr.devandate is null and rr.shipdate is null and convert(varchar(10),rr.chgdate,21) = convert(varchar(10),getdate(),21) ";
                //string query_received = " and rr.received is not null and rr.devandate is null and rr.shipdate is null and convert(varchar(10),rr.received,21) = convert(varchar(10),getdate(),21) ";
                //string query_processed = " and rr.devandate is not null and rr.shipdate is null and convert(varchar(10),rr.devandate,21) = convert(varchar(10),getdate(),21)";
                //string query_shipped = " and rr.shipdate is not null and convert(varchar(10),rr.shipdate,21) = convert(varchar(10),getdate(),21) ";




                string query_received = " AND rr.received IS NOT NULL AND rr.devandate IS NULL AND rr.shipdate IS NULL AND isdate(rr.received) = 1 AND rr.received >= getdate() - 7 ";
                string query_processed = " AND rr.devandate IS NOT NULL AND isdate(rr.devandate) = 1 AND rr.devandate >= getdate() - 7 ";



                string query_po_filter = " ";
                if (args != "0")
                    query_po_filter = " and r.pono in (" + args + ") ";


                string query = query_header.Replace("@@status@@", "'RECEIVED PENDING'").Replace("@@status_date@@", "rr.received") + query_po_filter + query_received
                             + " UNION "
                             + query_header.Replace("@@status@@", "'RECEIVED COMPLETE'").Replace("@@status_date@@", "rr.DevanDate") + query_po_filter + query_processed;

                List<QReslt> rs = new List<QReslt>();
                // ConvertDataTable<QReslt>(sg.getTableByCommande(query));
                DataTable dt = sg.getTableByCommande(query);
                for (int x = 0; x < dt.Rows.Count; x++)
                {
                    QReslt q = new QReslt();
                    q.pono = dt.Rows[x]["pono"].ToString();
                    q.ProductID = dt.Rows[x]["ProductID"].ToString();
                    q.sku = dt.Rows[x]["sku"].ToString();
                    q.upc = dt.Rows[x]["upc"].ToString();
                    q.packqty = decimal.Parse(dt.Rows[x]["packqty"].ToString());
                    q.overqty = decimal.Parse(dt.Rows[x]["overqty"].ToString());
                    q.shortqty = decimal.Parse(dt.Rows[x]["shortqty"].ToString());
                    q.damagedqty = decimal.Parse(dt.Rows[x]["damagedqty"].ToString());
                    q.quarantineqty = decimal.Parse(dt.Rows[x]["quarantineqty"].ToString());
                    q.qtyshipped = decimal.Parse(dt.Rows[x]["qtyshipped"].ToString());
                    q.location = dt.Rows[x]["location"].ToString();
                    q.status_date = dt.Rows[x]["status_date"].ToString();
                    q.status = dt.Rows[x]["status"].ToString();
                    rs.Add(q);
                }


                if (rs.Count() > 0)
                {
                    var ponos = rs.Select(p => new { p.pono, p.status, p.status_date }).Distinct();
                    foreach (var p in ponos)
                    {
                        LulusResponse l = new LulusResponse();

                        l.apiVersion = "v1";
                        l.context = "Mitco Purchase Status Update";
                        l.data = new Data();
                        l.data.number = p.pono;
                        l.data.status = p.status;

                        l.data.items = new List<Item>();

                        var products = rs.Where(r => r.pono == p.pono).Select(m => m.sku).Distinct();

                        foreach (var item in products)
                        {
                            Item i1 = new Item();
                            i1.productId = rs.Where(r => r.sku == item && r.pono == p.pono).FirstOrDefault().ProductID;
                            i1.skuId = rs.Where(r => r.sku == item && r.pono == p.pono).FirstOrDefault().sku;
                            i1.upc = rs.Where(r => r.sku == item && r.pono == p.pono).FirstOrDefault().upc;
                            i1.qty = new Qty();
                            decimal ChicoP = 0;
                            decimal VcP = 0;
                            decimal PennP = 0;
                            try
                            {
                                ChicoP = rs.Where(r => r.sku == item && r.location.ToLower() == "Chico".ToLower() && r.pono == p.pono).FirstOrDefault().packqty;
                            }
                            catch (Exception){ChicoP = 0;}
                            try
                            {
                                VcP = rs.Where(r => r.sku == item && r.location.ToLower() == "Vc".ToLower() && r.pono == p.pono).FirstOrDefault().packqty;
                            }
                            catch (Exception) { VcP = 0; }
                            try
                            {
                                PennP = rs.Where(r => r.sku == item && r.location.ToLower() == "Penn".ToLower() && r.pono == p.pono).FirstOrDefault().packqty;
                            }
                            catch (Exception) { PennP = 0; }

                            i1.qty.Chico = ChicoP;
                            i1.qty.Vc = VcP;
                            i1.qty.Penn = PennP;

                            l.data.items.Add(i1);
                        }

                        int cnt = int.Parse(sg.getTableByCommande("select count(*) cnt from api_importation_lulus_status where po = '" + l.data.number + "' and status ='" + p.status + "' and convert(varchar(10),status_date,21) = convert(varchar(10),GETDATE(),21)").Rows[0][0].ToString());
                        if (cnt == 0)
                        {
                            string body_cnt = SimpleJson.SerializeObject(l);
                            sg.Execute("INSERT INTO api_importation_lulus_status ([po],[status],[status_date],body_content) VALUES ('" + l.data.number + "','" + p.status + "',GETDATE(),'" + body_cnt + "')");
                            if (ConfigurationManager.AppSettings["enable_lulus_check_status"] == "1")
                            {
                                var client = new RestClient(endpoint);
                                client.Timeout = -1;
                                var request = new RestRequest(Method.POST);
                                request.AddHeader("Content-Type", "application/json");
                                request.AddHeader("Authorization", auth);
                                request.AddParameter("application/json", body_cnt, ParameterType.RequestBody);
                                IRestResponse response = client.Execute(request);
                                LUlog.Information("Response from LULUS API : " + response.Content);
                            }


                        }
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                Log.Error("sendStatus() - ERROR : " + ex.Message);
                return 0;
            }
        }


    }
}
