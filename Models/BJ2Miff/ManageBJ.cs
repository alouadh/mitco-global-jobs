﻿using EdiTools;
using FluentFTP;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using Serilog;
using Serilog.Core;
using Serilog.Sinks.LiteDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Scheduled.Task.Manager.Models.BJ2Miff
{
    class ManageBJ
    {
        private SGBD sg = new SGBD(ConfigurationManager.AppSettings["BJdb"]);



        public int EdiScrapper2(string shipID)
        {
            // var BJlog = new LoggerConfiguration().WriteTo.LiteDB("logs/BJ/BJlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            // var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();


            InternetExplorerDriverService service = InternetExplorerDriverService.CreateDefaultService();
            service.SuppressInitialDiagnosticInformation = true;
            InternetExplorerDriver driver = new InternetExplorerDriver(service);

            try
            {
                string BJloginURL = ConfigurationManager.AppSettings["BJloginURL"];
                string BJuserID = ConfigurationManager.AppSettings["BJuserID"];
                string BJpassword = ConfigurationManager.AppSettings["BJpassword"];
                string BJscrappURL = ConfigurationManager.AppSettings["BJscrappURL"].ToString() + shipID;

                driver.Navigate().GoToUrl(BJloginURL);

                Thread.Sleep(3000);

                driver.FindElement(By.CssSelector("#userID")).SendKeys(BJuserID);
                driver.FindElement(By.CssSelector("#password")).SendKeys(BJpassword);

                driver.FindElement(By.CssSelector("#userSubmit")).Click();

                Thread.Sleep(3000);

                driver.Navigate().GoToUrl(BJscrappURL);

                string mode = driver.FindElement(By.CssSelector("#detailTable" + shipID + " > tbody > tr:nth-child(1) > td.loadreportbody")).Text;

                string sql = "UPDATE blujay_message " +
                      "   SET Mode = '" + mode + "' " +
                      " WHERE ShipmentID = '" + shipID + "'";
                sg.Execute(sql);

                var rows = driver.FindElements(By.CssSelector("#shipmentTable" + shipID + " > tbody > tr"));
                string j = "1";
                for (int i = 1; i < rows.Count(); i++)
                {
                    string po = "";
                    string origin = "";
                    string dc = "";
                    try
                    {
                        origin = driver.FindElement(By.CssSelector("#shipmentTable" + shipID + " > tbody > tr:nth-child(" + i.ToString() + ") > td.resultrow" + j + ".summary-body > table > tbody > tr:nth-child(2) > td:nth-child(2)")).Text;
                        po = driver.FindElement(By.CssSelector("#shipmentTable" + shipID + " > tbody > tr:nth-child(" + i.ToString() + ") > td.resultrow" + j + ".summary-body > table > tbody > tr:nth-child(1) > td")).Text;
                        dc = driver.FindElement(By.CssSelector("#shipmentTable" + shipID + " > tbody > tr:nth-child(" + i.ToString() + ") > td.resultrow" + j + ".summary-body > table > tbody > tr:nth-child(3) > td:nth-child(2)")).Text;
                        // dt.Rows.Add(po, dc);
                        sql = "UPDATE blujay_message_po " +
                              " SET [destination] = '" + dc + "' " +
                              " ,[origin] = '" + origin + "' " +
                              " WHERE ShipmentID = '" + shipID + "' " +
                              " AND PONumber = '" + po + "'";
                        sg.Execute(sql);

                        int dcID = 0;
                        try
                        {
                            dcID = int.Parse(new String(dc.Split(',')[0].Where(Char.IsDigit).ToArray()));
                        }
                        catch (Exception ex)
                        {
                            BJlog.Error("EdiScrapper2 - dcID scrapp : " + ex.Message);
                        }
                        try
                        {
                            sg.Execute("update TransloadReceiptItem set Location = 'MEI" + dcID.ToString() + "', PieceDescription = '" + origin.Split(',')[0].Split(' ')[0] + "' where PurchaseOrder = '" + po + "' and TransloadReceiptId in (select r.Id from TransloadReceipt r where r.CustomerRefNumber2 = '" + shipID + "' )");
                        }
                        catch (Exception ex)
                        {
                            BJlog.Error("EdiScrapper2 - update TransloadReceiptItem set shipaddr : " + ex.Message);
                        }

                        if (j == "1")
                            j = "2";
                        else
                            j = "1";
                    }
                    catch (Exception ex)
                    {
                        BJlog.Error("EdiScrapper2 - ERROR : " + ex.Message);
                    }
                }
                //sql = "UPDATE blujay_imported_edi_file SET status = 1 WHERE ShipmentID = '" + shipID + "'";
                //sg.Execute(sql);
            }
            catch (Exception)
            {
                //BJlog.Error("EdiScrapper2 - ERROR : "+ ex.Message);
                // driver.Quit();
            }
            finally
            {
                driver.Quit();
            }

            return 1;
        }


        public static string IsNumeriC(string str, double eq)
        {
            try
            {
                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalSeparator = ".";
                provider.NumberGroupSeparator = ",";
                double doubleVal = Convert.ToDouble(str, provider);
                return Math.Round(doubleVal / eq, 2).ToString().Replace(",", ".");
            }
            catch (Exception)
            {
                return "0.00";
            }
        }


        public class DisplayBJ
        {
            private string total_cube_value, total_weight_value = "";
            public string CustomerName { get; set; } = "";
            public string Customerrefenrence { get; set; } = "";
            public string ShipmentID { get; set; } = "";
            public string MasterBillofLading { get; set; } = "";


            public string Notes { get; set; } = "";

            public string TenderID { get; set; } = "";

            public string PickupLocationName { get; set; } = "";
            public string PickupLocationCity { get; set; } = "";
            public string PickupLocationAddress { get; set; } = "";

            // Additionnal split ************************************
            public string PickupLocationShortAddress { get; set; } = "";
            public string PickupLocationAddressCode { get; set; } = "0";
            public string PickupLocationState { get; set; } = "";
            public string PickupLocationZipCode { get; set; } = "";
            public string PickupLocationCountry { get; set; } = "";
            public string PickupLocationContactName { get; set; } = "";
            public string PickupLocationCommunicationNumber { get; set; } = "";
            // ******************************************************
            public string PickupLocationContact { get; set; } = "";
            public string DeliveryLocationName { get; set; } = "";
            public string DeliveryLocationCity { get; set; } = "";
            public string DeliveryLocationAddress { get; set; } = "";
            public string DeliveryLocationContact { get; set; } = "";
            // Additionnal split ************************************
            public string DeliveryLocationAddressCode { get; set; } = "0";
            public string DeliveryLocationShortAddress { get; set; } = "";
            public string DeliveryLocationState { get; set; } = "";
            public string DeliveryLocationZipCode { get; set; } = "";
            public string DeliveryLocationCountry { get; set; } = "";
            public string DeliveryLocationContactName { get; set; } = "";
            public string DeliveryLocationCommunicationNumber { get; set; } = "";
            // ******************************************************
            public string Vessel { get; set; } = "";
            public string Carrier { get; set; } = "";
            public string Voyage { get; set; } = "";
            public string VesselETA { get; set; } = "";
            public string Seal { get; set; } = "";
            public string Container { get; set; } = "";
            public string Containersize { get; set; } = "";
            public string Mode { get; set; } = "";
            public string ContainerType { get; set; } = "";
            public string TotalCube
            {
                get
                {
                    return total_cube_value;
                }
                set
                {
                    total_cube_value = IsNumeriC(value, 35.315);
                }
            }
            public string TotalProductWeight
            {
                get
                {
                    return total_weight_value;
                }
                set
                {
                    total_weight_value = IsNumeriC(value, 2.205);
                }
            }
            // New ***
            public string FreightRate { get; set; } = "0";
            public string Charge { get; set; } = "0";
            public string LandingQuantity { get; set; } = "0";
            // ***
            public string CommodityType { get; set; } = "";
            public List<Pos> POs { get; set; }
            public string PlannedPickupDate { get; set; } = "";
            public string PlannedDeliveryDate { get; set; } = "";
        }

        public class Pos
        {
            private string cube_value, weight_value = "";

            public string ShipmentID { get; set; }
            public string PONumber { get; set; }
            public string quantity { get; set; }
            public string weight
            {
                get
                {
                    return weight_value;
                }
                set
                {
                    weight_value = IsNumeriC(value, 2.205);
                }
            }
            public string Cube
            {
                get
                {
                    return cube_value;
                }
                set
                {
                    cube_value = IsNumeriC(value, 35.315);
                }
            }
            public string destination { get; set; }
        }


        bool codeInUse = false;


        public static Logger BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();


        public int EdiImporter()
        {

            // var BJlog = new LoggerConfiguration().WriteTo.LiteDB("logs/BJ/BJlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            // var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();

            if (codeInUse == true)
            {
                BJlog.Error("EdiImporter TRY TO ENTER LAUNCH CODE");
                return 1;
            }

            try
            {
                codeInUse = true;
                string Host = ConfigurationManager.AppSettings["BJftpHost"];
                string Username = ConfigurationManager.AppSettings["BJftpUsername"];
                string Password = ConfigurationManager.AppSettings["BJftpPassword"];
                string FromFolder = ConfigurationManager.AppSettings["BJFromFolder"];
                string ToFolder = ConfigurationManager.AppSettings["BJToFolder"];



                FtpClient client = new FtpClient(Host);
                client.Credentials = new NetworkCredential(Username, Password);
                client.Connect();


                //FtpTools ft = new FtpTools(Host, Username, Password, FromFolder, ToFolder);

                string filter = DateTime.Now.AddDays(0).ToString("yyyyMMdd");
                var FromFiles = client.GetListing(FromFolder).Where(w => w.Name.Contains(filter));

                // ft.GetRemoteFilesOrFoldersList(2).Where(w => w.FullName.Contains(".edi") && w.LastWriteTime.ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd"));

                // client.Dispose();

                // var FromFiles = ft.GetRemoteFilesOrFoldersList(2).Where(w => w.FullName.Contains(".edi"));
                var filePaths = Directory.GetFiles(ToFolder, "*" + filter + "*.edi").ToList();

                List<string> filesAttr = new List<string>();
                // List<string> filesToProcess = new List<string>();

                EdiMapping ediMapping = EdiMapping.Load(ToFolder + "/" + "204MAP.xml");

                try
                {
                    foreach (var item in filePaths)
                    {
                        filesAttr.Add(Path.GetFileName(item));
                    }
                    foreach (var item in FromFiles)
                    {
                        if (filesAttr.Where(x => x == item.Name).Count() == 0)
                        {
                            try
                            {
                                // ft.DownloadFile(item);
                                // filesToProcess.Add(item.Name);


                                client.DownloadFile(ToFolder + "/" + item.Name, item.FullName);

                                string root = ToFolder + "/" + item.Name;
                                EdiDocument ediDocument = EdiDocument.Load(root);
                                var transactionSets = ediDocument.TransactionSets;

                                foreach (var transaction in transactionSets)
                                {
                                    XDocument xml = ediMapping.Map(transaction.Segments);
                                    var serializer = new XmlSerializer(typeof(TemplateXML204.mapping));
                                    TemplateXML204.mapping rslt = (TemplateXML204.mapping)serializer.Deserialize(xml.CreateReader());

                                    DisplayBJ dbj = new DisplayBJ();
                                    dbj.CustomerName = rslt.N1.N102;
                                    dbj.Customerrefenrence = rslt.N1.N104;
                                    dbj.ShipmentID = rslt.B2.B204;
                                    dbj.MasterBillofLading = "N/A";

                                    dbj.TenderID = rslt.L11[0].L1101;


                                    dbj.Notes = rslt.NTE.NTE02;


                                    dbj.PickupLocationName = rslt.S5loop[0].N1.N102;
                                    dbj.PickupLocationCity = rslt.S5loop[0].N4.N401;
                                    dbj.PickupLocationAddress = rslt.S5loop[0].N1.N102 + " " + rslt.S5loop[0].N3.N301 + " " + rslt.S5loop[0].N3.N302 + " " + rslt.S5loop[0].N4.N401 + ", " + rslt.S5loop[0].N4.N402 + " " + rslt.S5loop[0].N4.N403 + " " + rslt.S5loop[0].N4.N404;
                                    dbj.PickupLocationContact = rslt.S5loop[0].G61.G6102 + " " + rslt.S5loop[0].G61.G6104;
                                    // New Fields ******************************************************
                                    dbj.PickupLocationAddressCode = rslt.S5loop[0].N1.N104.ToString();
                                    dbj.PickupLocationShortAddress = rslt.S5loop[0].N3.N301;
                                    dbj.PickupLocationState = rslt.S5loop[0].N4.N402;
                                    dbj.PickupLocationZipCode = rslt.S5loop[0].N4.N403;
                                    dbj.PickupLocationCountry = rslt.S5loop[0].N4.N404;
                                    dbj.PickupLocationContactName = rslt.S5loop[0].G61.G6102;
                                    dbj.PickupLocationCommunicationNumber = rslt.S5loop[0].G61.G6104;
                                    // *****************************************************************
                                    dbj.DeliveryLocationName = rslt.S5loop[1].N1.N102;
                                    dbj.DeliveryLocationCity = rslt.S5loop[1].N4.N401;
                                    dbj.DeliveryLocationAddress = rslt.S5loop[1].N1.N102 + " " + rslt.S5loop[1].N3.N301 + " " + rslt.S5loop[1].N3.N302 + " " + rslt.S5loop[1].N4.N401 + ", " + rslt.S5loop[1].N4.N402 + " " + rslt.S5loop[1].N4.N403 + " " + rslt.S5loop[1].N4.N404;
                                    dbj.DeliveryLocationContact = rslt.S5loop[1].G61.G6102 + " " + rslt.S5loop[1].G61.G6104;
                                    // New Fields ******************************************************
                                    dbj.DeliveryLocationAddressCode = rslt.S5loop[1].N1.N104.ToString();
                                    dbj.DeliveryLocationShortAddress = rslt.S5loop[1].N3.N301;
                                    dbj.DeliveryLocationState = rslt.S5loop[1].N4.N402;
                                    dbj.DeliveryLocationZipCode = rslt.S5loop[1].N4.N403;
                                    dbj.DeliveryLocationCountry = rslt.S5loop[1].N4.N404;
                                    dbj.DeliveryLocationContactName = rslt.S5loop[1].G61.G6102;
                                    dbj.DeliveryLocationCommunicationNumber = rslt.S5loop[1].G61.G6104;
                                    // *****************************************************************
                                    dbj.Vessel = "N/A";
                                    dbj.Carrier = "N/A";
                                    dbj.Voyage = "N/A";
                                    dbj.VesselETA = "N/A";
                                    dbj.Seal = "N/A";
                                    dbj.Container = rslt.N7loop.N7.N702;
                                    dbj.Containersize = "N/A";
                                    dbj.Mode = "";
                                    dbj.ContainerType = "N/A";
                                    // New Fields **
                                    dbj.TotalCube = rslt.L3[0].L309;
                                    dbj.TotalProductWeight = rslt.L3[0].L301;
                                    dbj.FreightRate = rslt.L3[0].L303;
                                    dbj.Charge = rslt.L3[0].L305;
                                    dbj.LandingQuantity = rslt.L3[0].L311;
                                    // ***
                                    dbj.CommodityType = "FAK";
                                    dbj.PlannedPickupDate = rslt.S5loop[0].G62.G6202;
                                    dbj.PlannedDeliveryDate = rslt.S5loop[1].G62.G6202;
                                    dbj.POs = new List<Pos>();


                                    string sql = "INSERT INTO blujay_message " +
                                                 "  ( CustomerName " +
                                                 "  , Customerrefenrence " +
                                                 "  , ShipmentID " +
                                                 "  , MasterBillofLading " +
                                                 "  , PickupLocationName " +
                                                 "  , PickupLocationCity,PickupLocationAddress, PickupLocationContact" +
                                                 "  , DeliveryLocationName " +
                                                 "  , DeliveryLocationCity,DeliveryLocationAddress,DeliveryLocationContact " +
                                                 "  , Vessel " +
                                                 "  , Carrier " +
                                                 "  , Voyage " +
                                                 "  , VesselETA " +
                                                 "  , Seal " +
                                                 "  , Container " +
                                                 "  , Containersize " +
                                                 "  , Mode " +
                                                 "  , ContainerType " +
                                                 "  , Cube " +
                                                 "  , TotalProductWeight " +
                                                 "  , CommodityType " +
                                                 "  , PlannedPickupDate " +
                                                 "  , PlannedDeliveryDate" +
                                                 "  ,PickupLocationAddressCode,PickupLocationShortAddress,PickupLocationState,PickupLocationZipCode" +
                                                 "  ,PickupLocationCountry,PickupLocationContactName,PickupLocationCommunicationNumber,DeliveryLocationAddressCode" +
                                                 "  ,DeliveryLocationShortAddress,DeliveryLocationState,DeliveryLocationZipCode,DeliveryLocationCountry,DeliveryLocationContactName" +
                                                 "  ,DeliveryLocationCommunicationNumber,FreightRate,Charge,LandingQuantity,created,TenderID" +
                                                 ") values " +
                                                 "  ( " +
                                                 "    '" + dbj.CustomerName.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.Customerrefenrence.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.ShipmentID.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.MasterBillofLading.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.PickupLocationName.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.PickupLocationCity.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.PickupLocationAddress.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.PickupLocationContact.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.DeliveryLocationName.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.DeliveryLocationCity.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.DeliveryLocationAddress.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.DeliveryLocationContact.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.Vessel.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.Carrier.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.Voyage.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.VesselETA.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.Seal.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.Container.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.Containersize.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.Mode.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.ContainerType.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.TotalCube.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.TotalProductWeight.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.CommodityType.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.PlannedPickupDate.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.PlannedDeliveryDate.Replace("'", "''") + "'" +
                                                 "  , '" + dbj.PickupLocationAddressCode + "','" + dbj.PickupLocationShortAddress + "','" + dbj.PickupLocationState + "','" + dbj.PickupLocationZipCode + "'" +
                                                 "  , '" + dbj.PickupLocationCountry + "','" + dbj.PickupLocationContactName + "','" + dbj.PickupLocationCommunicationNumber + "','" + dbj.DeliveryLocationAddressCode + "'" +
                                                 "  , '" + dbj.DeliveryLocationShortAddress + "','" + dbj.DeliveryLocationState + "','" + dbj.DeliveryLocationZipCode + "','" + dbj.DeliveryLocationCountry + "','" + dbj.DeliveryLocationContactName + "'" +
                                                 "  , '" + dbj.DeliveryLocationCommunicationNumber + "','" + dbj.FreightRate + "','" + dbj.Charge + "','" + dbj.LandingQuantity + "',getdate(),'" + dbj.TenderID + "') ";

                                    try
                                    {
                                        sg.Execute(sql);
                                    }
                                    catch (Exception ex)
                                    {
                                        BJlog.Error("blujay_message insert FAILED - ERROR", ex.Message);
                                    }



                                    // managing external addresses 
                                    try
                                    {
                                        sql = @" insert into TransloadAddress
 (Code, OrganizationName, Address, City, State, PostalCode, Phone, WebPage, CountryId, Created, Updated, Contact, Active,Enabled )  
 (  
 SELECT        
 DISTINCT    
SUBSTRING(b.PickupLocationName,1,3)+SUBSTRING(b.PickupLocationCity,1,3)+ FORMAT((select count(a.id)+1 from TransloadAddress a where a.Code like SUBSTRING(b.PickupLocationName,1,3)+SUBSTRING(b.PickupLocationCity,1,3)+'%' ),'000'), 
 b.PickupLocationName,  
 b.PickupLocationShortAddress,   
 b.PickupLocationCity,   
 b.PickupLocationState,   
 b.PickupLocationZipCode,
 b.PickupLocationCommunicationNumber,  
 convert(varchar,CONVERT(numeric,b.PickupLocationAddressCode)),  
 (select TOP 1 ID from Country where Iso3 = b.PickupLocationCountry) ,
  getdate(), 
 getdate(),  
 b.PickupLocationContactName,   
 1,  
 1
 FROM blujay_message AS b  
 where b.PickupLocationAddressCode is not null  
 and CONVERT(numeric,b.PickupLocationAddressCode) not in (select top 1 CONVERT(numeric,WebPage) from TransloadAddress where isnumeric(WebPage) =1 and CONVERT(numeric,WebPage) = CONVERT(numeric,b.PickupLocationAddressCode))  
 and isnumeric(b.PickupLocationAddressCode)=1  
 )  ";
                                        sg.Execute(sql);
                                    }
                                    catch (Exception ex)
                                    {
                                        BJlog.Error("Addresses Sync FAILED - ERROR", ex.Message);
                                    }
                                    // *** managing external addresses 
                                    string currentOrdID, currentRcvID;
                                    if (dbj.Container == "N/A")
                                    {
                                        // Creating orders ------ 

                                        sql = "select count(*) cnt from TransloadOrder where CustomerRefNumber2 ='" + dbj.ShipmentID + "'";
                                        DataTable dtOrd = sg.getTableByCommande(sql);
                                        if (dtOrd.Rows[0][0].ToString() == "0")
                                        {
                                            //Insert order
                                            sql = @" insert into TransloadOrder (

         CustomerId
		,OrderTypeId
		,OrderDate
		,Created
		,Updated
		,Enabled
		,ServiceId
		,ModeId
		,CustomerRefNumber
		,CustomerRefNumber2
	    ,MasterBill
) values
    (
         112
        , 5
        , getdate()
        , getdate()
        , getdate()
        , 1
        , 9
        , 1
        , '@shipmentid@'
        , '@shipmentid@'
        , '@MasterBill@'
    )"; 
                                            try
                                            {
                                                sg.Execute(sql.Replace("@shipmentid@", dbj.ShipmentID.Replace("'", "''")).Replace("@MasterBill@", dbj.MasterBillofLading.Replace("'", "''")));
                                            }
                                            catch (Exception ex)
                                            {
                                                BJlog.Error("INSERT INTO TransloadOrder - ERROR", ex.Message);
                                            }


                                            sql = "select top 1 id from TransloadOrder where CustomerRefNumber2 = '" + dbj.ShipmentID + "' order by id desc";
                                            dtOrd = sg.getTableByCommande(sql);
                                            currentOrdID = dtOrd.Rows[0][0].ToString();

                                            sql = @"insert into TransloadReceipt
(
	 CustomerId
	,TransloadOrderId
	,EquipmentRef
	,CustomerRefNumber
	,CustomerRefNumber2
	,Created
	,Updated
	,Enabled
	,TotalWeight1
	,TotalCube1
	,WeightType1
	,CubeType1
	,IsDefaultWeightTypeKgs
	,PickupLocationId
	,StatusId
	,ServiceId
	,ModeId
	,ShipperId
	,IsCanceled
	,PlanDate
	,ItemsCount
	,MasterBill
) values
(
	112 
	, '@TransloadOrderId@' 
	,'N/A' 
	,'@shipmentid@' 
	,'@shipmentid@' 
	,getdate() 
	,getdate() 
	,1 
	,'@TotalWeight@' 
	,'@TotalCube@' 
	,'KGS' 
	,'CBM'
    , 1
	,(select top 1 Id from TransloadAddress where code = '@TransloadAddress@') 
	,2 
	,9 
	,1 
	,(select top 1 Id from TransloadAddress where code = '@TransloadAddress@') 
	,0 
	,'@PlanDate@' 
	,'@ItemsCount@' 
	,'@MasterBill@'
)";

                                            sql = sql.Replace("@TransloadOrderId@", currentOrdID)
                                                .Replace("@shipmentid@", dbj.ShipmentID.Replace("'", "''"))
                                                .Replace("@TotalWeight@", dbj.TotalProductWeight.Replace("'", "''"))
                                                .Replace("@TotalCube@", dbj.TotalCube.Replace("'", "''"))
                                                .Replace("@TransloadAddress@", dbj.PickupLocationAddressCode).Replace("@PlanDate@", "")
                                                .Replace("@ItemsCount@", dbj.LandingQuantity.Replace("'", "''"))
                                                .Replace("@MasterBill@", dbj.MasterBillofLading.Replace("'", "''"))
                                                .Replace("@PlanDate@", dbj.PlannedDeliveryDate.Replace("'", "''"));

                                            try
                                            {
                                                sg.Execute(sql);
                                            }
                                            catch (Exception ex)
                                            {
                                                BJlog.Error("INSERT INTO TransloadReceipt - ERROR 1 {@Ex}", ex);
                                            }

                                            sql = "select top 1 id from TransloadReceipt where TransloadOrderId = '" + currentOrdID + "'  order by id desc";
                                            dtOrd = sg.getTableByCommande(sql);
                                            currentRcvID = dtOrd.Rows[0][0].ToString();
                                            int line = 1;
                                            foreach (var oid in rslt.S5loop[0].OID)
                                            {
                                                Pos p = new Pos();
                                                p.PONumber = oid.OID02;
                                                p.quantity = oid.OID05;
                                                p.weight = oid.OID07;
                                                p.Cube = oid.OID09;
                                                p.ShipmentID = dbj.ShipmentID;
                                                p.destination = "";
                                                // dbj.POs.Add(p);
                                                sql = "INSERT INTO blujay_message_po " +
                                                      " ([ShipmentID] " +
                                                      " , [PONumber] " +
                                                      " , [quantity] " +
                                                      " , [weight] " +
                                                      " , [Cube] " +
                                                      " , [destination]) " +
                                                      " VALUES                     " +
                                                      " ( '" + p.ShipmentID + "'" +
                                                      " , '" + p.PONumber + "'" +
                                                      " , '" + p.quantity + "'" +
                                                      " , '" + p.weight + "'" +
                                                      " , '" + p.Cube + "'" +
                                                      " , '" + p.destination + "'" +
                                                      " )";
                                                try
                                                {
                                                    sg.Execute(sql);
                                                }
                                                catch (Exception ex)
                                                {
                                                    BJlog.Error("INSERT INTO blujay_message_po - ERROR", ex.Message);
                                                }

                                                sql = @"insert into TransloadReceiptItem
(
	TransloadReceiptId
	,Description
	,PurchaseOrder
	,Weight
	,WeightType
	,Cube
	,CubeType
	,UnitCount
	,Created
	,Enabled
	,CreatedBy
	,Pieces
) values
(
'@currentRcvID@'
,'@ShipmentID@'
,'@PONumber@'
,convert(float, '@weight@')
,'KGM'
,convert(float, '@Cube@')
,'CBM'
,'@quantity@'
,getdate()
,1
,'204'
,'@quantity@'
)";
                                                sql = sql.Replace("@currentRcvID@", currentRcvID)
                                                    .Replace("@ShipmentID@", p.ShipmentID)
                                                    .Replace("@PONumber@", p.PONumber)
                                                    .Replace("@weight@", p.weight)
                                                    .Replace("@Cube@", p.Cube)
                                                    .Replace("@quantity@", p.quantity);

                                                try
                                                {
                                                    sg.Execute(sql);
                                                }
                                                catch (Exception ex)
                                                {
                                                    BJlog.Error("INSERT INTO recvitems - ERROR", ex.Message);
                                                }
                                                line += 1;
                                            }

                                        }


                                        //else
                                        //{
                                        //    currentRcvID = "";
                                        //    sql = "select top 1 id from TransloadOrder where CustomerRefNumber2 ='" + dbj.ShipmentID + "' order by id desc";
                                        //    dtOrd = sg.getTableByCommande(sql);
                                        //    currentOrdID = dtOrd.Rows[0][0].ToString();
                                        //    // is received or devanned
                                        //    string isRorD = sg.getTableByCommande("select count(*)  from mitcoltd.dbo.receive where orderid = " + currentOrdID + " and (received is not null or devandate is not null) and customerid = 'meimi'").Rows[0][0].ToString();
                                        //    if (isRorD == "0")
                                        //    {
                                        //        // Insert new Receive 

                                        //        try
                                        //        {


                                        //            string isZeroPayLoad = "''";
                                        //            if (dbj.Notes.ToUpper().Contains("ZEROPAYLOAD"))
                                        //            {
                                        //                isZeroPayLoad = "'ZEROPAYLOAD'";

                                        //            }

                                        //            sql = "insert into mitcoltd.dbo.receive " +
                                        //            " ( orderid, dodate, received, devandate, lastfreedate, shipdate, ATD, originshipper, origcode, cancelflag, is210Sent, department, customerid, billtoid, receivedbyid, equipmentid, masterbill, equiprefno, sealno, estavaildate,  " +
                                        //            "  mitcopickup, inwhlocid, releaseno, pickuplocid, status, originid, destid, pieces, piecetype, cube, cubetype, weight, weighttype, custrefno, invoiceno, invoicedate, rbillrate, rbillcalc, rFuelSur, rFuelCalc, rACCDescrip, rAccAmount,  " +
                                        //            " rAccCalc, rACCDescrip1, rAccAmount1, rAccCalc1, rACCDescrip2, rAccAmount2, rAccCalc2, invexport, puvendorid, puvendrefno, puvendcost, custnote, Internalnote, deleteflagheader, Receivedby, Checkedby, userid, chgdate,  " +
                                        //            " sent210Date, uflag, isRamp210sent, Ramp210date, ActInvoiceDate, isIIFsent, IIFdate, Terminal, DispatchedDate, osdtrue, My856, ScheduledDeliveryDate, ReleaseTime, Hot, My856Updated, WorkByDate,  " +
                                        //            " TriniumScheduled, PartialReceive, CustomerDepartment, CustomerBuyer, FirstReceiveDate, LastReceiveDate, CFS, ExtensionDate, ScheduledPUDate, ActualPUDate, ActualDeliveredDate, RequestedShipDate, CancelDate,  " +
                                        //            " PickupLocationID, Pickup, OrderStatus, TotalPallets, TotalCartons, ActualPallets, ActualCartons, CancelCode, CancelNote, PUWindowStart, PUWindowEnd, PickupNote, CarrierID, ShippingTerms, FreightForwarder, POL, POD,  " +
                                        //            " PODCountryID, CustomsCleared, CustomsClearedDateTime, CountryISO, DispatchOrderFile, custrefno2, hazmat, om_status, om_order_date, om_IsInBJ, om_isReviewed) " +
                                        //            " select top 1  orderid, dodate, received, devandate, lastfreedate, shipdate, ATD, originshipper, origcode, '0', is210Sent, department, customerid, billtoid, receivedbyid, equipmentid, masterbill, equiprefno, sealno, estavaildate,  " +
                                        //            " mitcopickup, inwhlocid, releaseno, (select top 1 addrid from mitcoltd.dbo.address where  ISNUMERIC(traceurl)=1 and CONVERT(numeric,traceurl) = CONVERT(numeric,'" + dbj.PickupLocationAddressCode + "') and traceurl is not null), status, originid, destid, pieces, piecetype, cube, cubetype, weight, weighttype, " + isZeroPayLoad + ", invoiceno, invoicedate, rbillrate, rbillcalc, rFuelSur, rFuelCalc, rACCDescrip, rAccAmount,  " +
                                        //            " rAccCalc, rACCDescrip1, rAccAmount1, rAccCalc1, rACCDescrip2, rAccAmount2, rAccCalc2, invexport, puvendorid, puvendrefno, puvendcost, custnote, Internalnote, deleteflagheader, Receivedby, Checkedby, userid, getdate(),  " +
                                        //            " sent210Date, uflag, isRamp210sent, Ramp210date, ActInvoiceDate, isIIFsent, IIFdate, Terminal, DispatchedDate, osdtrue, My856, ScheduledDeliveryDate, ReleaseTime, Hot, My856Updated, WorkByDate,  " +
                                        //            " TriniumScheduled, PartialReceive, CustomerDepartment, CustomerBuyer, FirstReceiveDate, LastReceiveDate, CFS, ExtensionDate, ScheduledPUDate, ActualPUDate, ActualDeliveredDate, RequestedShipDate, CancelDate,  " +
                                        //            " PickupLocationID, Pickup, OrderStatus, TotalPallets, TotalCartons, ActualPallets, ActualCartons, CancelCode, CancelNote, PUWindowStart, PUWindowEnd, PickupNote, CarrierID, ShippingTerms, FreightForwarder, POL, POD, " +
                                        //            " PODCountryID, CustomsCleared, '" + dbj.PlannedDeliveryDate.Replace("'", "''") + "', CountryISO, DispatchOrderFile, '" + dbj.ShipmentID + "', hazmat, om_status, om_order_date, om_IsInBJ, om_isReviewed from mitcoltd.dbo.receive where orderid = '" + currentOrdID + "' order by id desc";

                                        //            // BJlog.Information(sql);

                                        //            sg.Execute(sql);
                                        //        }
                                        //        catch (Exception ex)
                                        //        {
                                        //            BJlog.Error("INSERT INTO mitcoltd.dbo.receive - ERROR 2 {@Ex}", ex);
                                        //        }

                                        //        sql = "select top 1 id from mitcoltd.dbo.receive where orderid = '" + currentOrdID + "' order by id desc";
                                        //        dtOrd = sg.getTableByCommande(sql);
                                        //        currentRcvID = dtOrd.Rows[0][0].ToString();

                                        //        // Update Old Receive **********************************
                                        //        try
                                        //        {

                                        //            sql = "update mitcoltd.dbo.receive set cancelflag='1',custrefno='Canceled' where orderid = '" + currentOrdID + "' and id <> '" + currentRcvID + "'";
                                        //            sg.Execute(sql);
                                        //            sg.Execute("update mitcoltd.dbo.recvitems set deleteflag = '1',cancelflag = '1' where orderid = '" + currentOrdID + "' ");
                                        //        }
                                        //        catch (Exception ex)
                                        //        {
                                        //            BJlog.Error("update mitcoltd.dbo.receive set cancelflag - ERROR", ex.Message);
                                        //        }
                                        //        // *****************************************************

                                        //        // *******************************************

                                        //        try
                                        //        {
                                        //            sql = "update mitcoltd.dbo.orders set shiptoid = '" + dbj.DeliveryLocationCity.Replace("'", "''") + "', originid=(select top 1 addrid from mitcoltd.dbo.address where  ISNUMERIC(traceurl)=1 and traceurl is not null and CONVERT(numeric,traceurl) = CONVERT(numeric,'" + dbj.PickupLocationAddressCode + "') )  " +
                                        //            ",weight='" + dbj.TotalProductWeight.Replace("'", "''") + "'" +
                                        //            ",cube='" + dbj.TotalCube.Replace("'", "''") + "'" +
                                        //            " where orderid = '" + currentOrdID + "' ";
                                        //            sg.Execute(sql);
                                        //        }
                                        //        catch (Exception ex)
                                        //        {
                                        //            BJlog.Error("Update shiptoid orders FAILED - ERROR : " + ex.Message);
                                        //        }

                                        //        int line = 1;
                                        //        foreach (var oid in rslt.S5loop[0].OID)
                                        //        {
                                        //            Pos p = new Pos();
                                        //            p.PONumber = oid.OID02;
                                        //            p.quantity = oid.OID05;
                                        //            p.weight = oid.OID07;
                                        //            p.Cube = oid.OID09;
                                        //            p.ShipmentID = dbj.ShipmentID;
                                        //            p.destination = "";
                                        //            // dbj.POs.Add(p);
                                        //            try
                                        //            {
                                        //                sql = "INSERT INTO blujay_message_po " +
                                        //                  " ([ShipmentID] " +
                                        //                  " , [PONumber] " +
                                        //                  " , [quantity] " +
                                        //                  " , [weight] " +
                                        //                  " , [Cube] " +
                                        //                  " , [destination]) " +
                                        //                  " VALUES                     " +
                                        //                  " ( '" + p.ShipmentID + "'" +
                                        //                  " , '" + p.PONumber + "'" +
                                        //                  " , '" + p.quantity + "'" +
                                        //                  " , '" + p.weight + "'" +
                                        //                  " , '" + p.Cube + "'" +
                                        //                  " , '" + p.destination + "'" +
                                        //                  " )";
                                        //                sg.Execute(sql);

                                        //            }
                                        //            catch (Exception ex)
                                        //            {
                                        //                BJlog.Error("Update blujay_message_po FAILED - ERROR : " + ex.Message);
                                        //            }

                                        //            try
                                        //            {
                                        //                sql = "  insert into mitcoltd.dbo.recvitems " +
                                        //                     " ( " +
                                        //                     "  line,orderid,recvid,descrip " +
                                        //                     "  , pono " +
                                        //                     "  , packqty " +
                                        //                     "  , weight,weighttype " +
                                        //                     "  , cube,cubetype " +
                                        //                     "  , shipaddr " +
                                        //                     "  , chgdate " +
                                        //                     "  , userid, deleteflag, adjustment, itemqty, CustomerNotes " +
                                        //                     " ) values " +
                                        //                     " ( " +
                                        //                     "  '" + line.ToString() + "','" + currentOrdID + "','" + currentRcvID + "','" + p.ShipmentID + "', " +
                                        //                     "  '" + p.PONumber + "', " +
                                        //                     "  convert(float, '" + p.quantity + "'), " +
                                        //                     "  convert(float, '" + p.weight + "'),'KGM', " +
                                        //                     "  convert(float, '" + p.Cube + "'),'CBM', " +
                                        //                     "  '', " +
                                        //                     "  getdate(), " +
                                        //                     "  204, 0, 0, '" + p.quantity + "', '' " +
                                        //                     " ) ";
                                        //                sg.Execute(sql);
                                        //            }
                                        //            catch (Exception ex)
                                        //            {
                                        //                BJlog.Error("insert into mitcoltd.dbo.recvitems FAILED - ERROR : " + ex.Message);
                                        //            }

                                        //            line += 1;
                                        //        }
                                        //        try
                                        //        {
                                        //            sg.Execute("update mitcoltd.dbo.blujay_orders_status set isReviewed = 1 where voyageid='" + currentRcvID + "'");
                                        //            sg.Execute("update mitcoltd.dbo.receive set om_isReviewed = 1 where orderid='" + currentOrdID + "'");

                                        //        }
                                        //        catch (Exception ex)
                                        //        {
                                        //            BJlog.Error("Update blujay_orders_status FAILED - ERROR : " + ex.Message);
                                        //        }
                                        //    }
                                        //    else
                                        //    {
                                        //        try
                                        //        {
                                        //            sg.Execute("update mitcoltd.dbo.receive set om_isReviewed = 1 where orderid='" + currentOrdID + "'");
                                        //        }
                                        //        catch (Exception ex)
                                        //        {
                                        //            BJlog.Error("update mitcoltd.dbo.receive received-devanned FAILED - ERROR : " + ex.Message);
                                        //        }

                                        //    }



                                        //}
                                    }

                                    try
                                    {
                                        sql = "INSERT INTO blujay_imported_edi_file " +
                                          " ([edi_file_name] " +
                                          " , [created] " +
                                          " , [ShipmentID] " +
                                          " , [status]) " +
                                          " VALUES " +
                                          " ('" + item.Name + "'" +
                                          " , getdate()                   " +
                                          " , '" + dbj.ShipmentID + "'" +
                                          " , 0 " +
                                          " ) ";
                                        sg.Execute(sql);
                                    }
                                    catch (Exception ex)
                                    {
                                        BJlog.Error("INSERT blujay_imported_edi_file FAILED - ERROR : " + ex.Message);
                                    }

                                    
                                    try
                                    {
                                        sg.Execute(@"update TransloadReceipt
set CustomerRefNumber2 = b.ShipmentID
from TransloadReceipt t, blujay_message b
where t.CustomerId = 112
and isnull(t.CustomerRefNumber2,'') = ''
and SUBSTRING(t.EquipmentRef, 1, 10) = SUBSTRING(b.Container, 1, 10)

update TransloadOrder
set CustomerRefNumber2 = r.CustomerRefNumber2
from 
TransloadReceipt r,TransloadOrder o
where r.TransloadOrderId = o.id
and r.CustomerRefNumber2 is not null
and o.CustomerRefNumber2 is null
and len(r.CustomerRefNumber2)>0 
and r.CustomerId = 112 ");

                                    }
                                    catch (Exception ex)
                                    {
                                        BJlog.Error("update TransloadReceipt CustomerRefNumber2 - ERROR : " + ex.Message);
                                    }






                                    try
                                    {
                                        var scrap = EdiScrapper2(dbj.ShipmentID);
                                    }
                                    catch (Exception ex)
                                    {
                                        BJlog.Error("EdiScrapper2 FAILED - ERROR : " + ex.Message);
                                    }
                                }
                            }
                            catch (System.Exception ex)
                            {
                                BJlog.Error("EdiReader SaveChanges - ERROR :" + ex.Message);
                            }
                        }
                    }

                }
                catch (System.Exception ex)
                {
                    BJlog.Error("EdiReader ListingFromFolders - ERROR : " + ex.Message);
                }
                codeInUse = false;
                return 1;
            }
            catch (Exception ex)
            {
                BJlog.Error("EdiReader - ERROR : " + ex.Message);
                codeInUse = false;
                return 0;
            }
        }
    }
}
