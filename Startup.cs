﻿using Hangfire;
using Hangfire.Heartbeat;
using Hangfire.Heartbeat.Server;
using Hangfire.JobsLogger;
using Hangfire.Server;
using Hangfire.Storage.SQLite;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Scheduled.Task.Manager.Models;
using Scheduled.Task.Manager.Models.BJ2Miff;
using Scheduled.Task.Manager.Models.BJscrapper;
using Scheduled.Task.Manager.Models.MeijerDrayage;
using Scheduled.Task.Manager.Models.Miff_Pickup_Locations;
using Scheduled.Task.Manager.Models.Miff2BJ;
using Scheduled.Task.Manager.Models.Miff2Lulus;
using Scheduled.Task.Manager.Models.Miff2Trinium;
using Scheduled.Task.Manager.Models.MiffStatus;
using Scheduled.Task.Manager.Models.studio_pull;
using System;
using System.Configuration;

namespace Scheduled.Task.Manager
{
    class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSQLiteStorage()
                .UseHeartbeatPage(checkInterval: TimeSpan.FromSeconds(60))
                );
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseHangfireServer(additionalProcesses: new[] { new ProcessMonitor(checkInterval: TimeSpan.FromSeconds(30)) });
            app.UseHangfireDashboard(string.Empty);


            // ######################################################################################
            ManageBJ m = new ManageBJ();
            RecurringJob.AddOrUpdate("BLUJAY", () => m.EdiImporter(), "0 0 */2 ? * *");
            //m.EdiImporter();
            // ######################################################################################


            //ManageMEIDRG mmd = new ManageMEIDRG();
            //RecurringJob.AddOrUpdate("MEI-DRAYAGE", () => mmd.updateMeijerTables(), "0 0 */5 ? * *");

            //SendLulusStatus ml = new SendLulusStatus();
            //RecurringJob.AddOrUpdate("LULUS-STATUS", () => ml.sendStatusGlobal(ConfigurationManager.AppSettings["lulus_check_status_args_prod"],
            //                                                                   ConfigurationManager.AppSettings["lulus_end_point_prod"],
            //                                                                   ConfigurationManager.AppSettings["lulus_authorization_prod"],
            //                                                                   ConfigurationManager.AppSettings["lulus_connection_prod"]),
            //                                                                   "0 */8 * ? * *");

            // ######################################################################################
            ManagerM2B m2b = new ManagerM2B();
            RecurringJob.AddOrUpdate("Miff2BJ", () => m2b.SendRcv2BJ(), "0 */40 * ? * *");
            // m2b.SendRcv2BJ();
            // ######################################################################################


            //StudioPull sp = new StudioPull();
            //RecurringJob.AddOrUpdate("Lulus-StudioPull", () => sp.CheckFiles(), "0 */30 * ? * *");


            //MiffStatus mfs = new MiffStatus();
            //RecurringJob.AddOrUpdate("Miff-Update-Status", () => mfs.UpdateMiffStatusGlobal(), "0 */15 * ? * *");

            //BJscrapper bjs = new BJscrapper();
            //RecurringJob.AddOrUpdate("BLUJAY-CHARGES", () => bjs.getCharges(), "0 0 */3 ? * *");


            //PickupLocations pl = new PickupLocations();
            //RecurringJob.AddOrUpdate("API-ETA-LOCATIONS", () => pl.getLocations(), "0 0 12 * * ?");

            //M2Trinium m2t = new M2Trinium();
            //RecurringJob.AddOrUpdate("Dispatch TRINIUM 204 ", () => m2t.DispatchFiles2TR(), "0 0 */1 ? * *");





            //m2b.SendRcv2BJ();
            //EdiTester e = new EdiTester();


            //StudioPull sp = new StudioPull();
            //sp.CheckFiles();

            //BJscrapper bjs = new BJscrapper();
            //bjs.getCharges();

            //PickupLocations pl = new PickupLocations();
            //pl.getLocations();


            //M2Trinium m2t = new M2Trinium();
            //m2t.DispatchFiles2TR();



        }

    }
}
