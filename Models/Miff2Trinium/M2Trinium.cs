﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace Scheduled.Task.Manager.Models.Miff2Trinium
{
    class M2Trinium
    {

        SGBD sg = new SGBD(ConfigurationManager.AppSettings["BJdb"]);


        // Accept 990 To BJ
        public string get204()
        {
            string edibody = @"ISA*00*          *00*          *ZZ*MITCO          *ZZ*SEAMOUNT       *@@date@@*@@time@@*U*00401*598017091*0*P*~
GS*01*MITCO*SEAMOUNT*@@date@@*@@time@@*598017220*X*004010
ST*204*0001
B2****@@recvid@@**CC
B2A*@@purposeCode@@
L11*@@masterbill@@*BM
L11*@@vessel@@*ABS
L11*@@voyageid@@*V3
AT5*ED
NTE*ZZZ*Vendor Charges : @@puvendcost@@ $
N7**@@equiprefno@@*****************@@terms@@**@@equipmentid@@
S5*1*LD*@@totalweight@@*K*@@totalqty@@**@@totalcube@@*X
N1*SF*@@SFcompany@@*ZZ*@@pickuplocid@@
N3*@@SFaddress@@
N4*@@SFcity@@*@@SFstate@@*@@SFcode@@*US
G61*ZZ**TE*
S5*2*LD*@@totalweight@@*K*@@totalqty@@**@@totalcube@@*X
N1*ST*@@STcompany@@*ZZ*@@inwhlocid@@
N3*@@STaddress@@
N4*@@STcity@@*@@STstate@@*@@STcode@@*US
G61*ZZ**TE*
L3*@@totalweight@@********@@totalcube@@*X*@@totalqty@@*K
SE*21*0001
GE*1*598017220
IEA*1*598017091";
            return edibody;
        }


        public int DispatchFiles2TR()
        {
            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJEDIlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();


            string mtcoFtp = ConfigurationManager.AppSettings["mitcoFtpFolder"];

            try
            {
                string datenow = DateTime.Now.ToString("yyMMdd");
                string timenow = DateTime.Now.ToString("HHmm");
                string fulldatenow = DateTime.Now.ToString("yyyyMMdd");
                string editxt = get204();
                string sql = @"SELECT 
	 recvid
	,rtrim(isnull(masterbill,'')) masterbill
	,rtrim((select top 1 Vessel from mitcoltd.dbo.orders o where o.orderid = r.orderid)) vessel
	,rtrim(equiprefno) equiprefno
	,rtrim(isnull((select top 1 newequipment from mitcoltd.dbo.equipment e where e.equipmentid = r.equipmentid),'53')) equipmentid
	,isnull((select sum(weight) from mitcoltd.dbo.recvitems ri where ri.recvid = r.recvid and isnull(ri.cancelflag,'0')<> '1'),0) totalweight
	,isnull((select sum(packqty) from mitcoltd.dbo.recvitems ri where ri.recvid = r.recvid and isnull(ri.cancelflag,'0')<> '1'),0) totalqty
	,isnull((select sum(cube) from mitcoltd.dbo.recvitems ri where ri.recvid = r.recvid and isnull(ri.cancelflag,'0')<> '1'),0) totalcube
	,isnull(puvendcost,0) puvendcost
	,rtrim(isnull(ShippingTerms,'CY')) terms
	,rtrim(r.DispatchedDate) DispatchedDate
	,rtrim(isnull(r.pickuplocid,'')) pickuplocid
	,isnull((select TOP 1 '04' from mitcoltd.dbo.edi_exchange_tracer eet where eet.status = 'TR204' and eet.recvid = r.recvid and isnull(eet.lastValue,'') = isnull(r.DispatchedDate,'')  order by id desc),'00') purposeCode
	,rtrim(isnull((select top 1 rtrim(isnull(addr.company,'')) from mitcoltd.dbo.address addr where addrid = r.pickuplocid),'')) SFcompany
	,rtrim(isnull((select top 1 rtrim(isnull(addr.address,'')) from mitcoltd.dbo.address addr where addrid = r.pickuplocid),'')) SFaddress
	,rtrim(isnull((select top 1 rtrim(isnull(addr.city,'')) from mitcoltd.dbo.address addr where addrid = r.pickuplocid),'')) SFcity
	,rtrim(isnull((select top 1 rtrim(isnull(addr.state,'')) from mitcoltd.dbo.address addr where addrid = r.pickuplocid),'')) SFstate
	,rtrim(isnull((select top 1 rtrim(isnull(addr.postcode,'')) from mitcoltd.dbo.address addr where addrid = r.pickuplocid),'')) SFcode
	,rtrim(r.inwhlocid) inwhlocid
	,rtrim(isnull((select top 1 rtrim(isnull(addr.company,'')) from mitcoltd.dbo.address addr where addrid = r.inwhlocid),'')) STcompany
	,rtrim(isnull((select top 1 rtrim(isnull(addr.address,'')) from mitcoltd.dbo.address addr where addrid = r.inwhlocid),'')) STaddress
	,rtrim(isnull((select top 1 rtrim(isnull(addr.city,'')) from mitcoltd.dbo.address addr where addrid = r.inwhlocid),'')) STcity
	,rtrim(isnull((select top 1 rtrim(isnull(addr.state,'')) from mitcoltd.dbo.address addr where addrid = r.inwhlocid),'')) STstate
	,rtrim(isnull((select top 1 rtrim(isnull(addr.postcode,'')) from mitcoltd.dbo.address addr where addrid = r.inwhlocid),'')) STcode
FROM mitcoltd.dbo.receive r
WHERE om_order_date > '2021-01-01'
	AND r.DispatchedDate is not null
	AND isnull(ShippingTerms,'') not in ('LTL','LCL')
	AND (select count(eet.recvid) from mitcoltd.dbo.edi_exchange_tracer eet where eet.status = 'TR204' and eet.recvid = r.recvid and isnull(eet.lastValue,'') = isnull(r.DispatchedDate,'') ) = 0
ORDER BY id DESC
";
                DataTable dt = sg.getTableByCommande(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string filename = mtcoFtp + "MTDI-204-TRM-" + dt.Rows[i]["recvid"].ToString() + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".edi";

                    string vessname = dt.Rows[i]["vessel"].ToString();
                    var varr = vessname.Split(" ");
                    string vessnum = "";
                    foreach (var item in varr)
                    {
                        bool containsInt = item.Any(char.IsDigit);
                        vessnum = item;
                    }

                    try
                    {
                        vessname = vessname.Replace(vessnum, "");
                    }
                    catch (Exception)
                    {

                    }
                    

                    File.WriteAllText(filename, editxt
                        .Replace("@@date@@", datenow)
                        .Replace("@@time@@", timenow)
                        .Replace("@@recvid@@", dt.Rows[i]["recvid"].ToString())
                        .Replace("@@purposeCode@@", dt.Rows[i]["purposeCode"].ToString())
                        .Replace("@@masterbill@@", dt.Rows[i]["masterbill"].ToString())

                        .Replace("@@vessel@@", vessname)

                        .Replace("@@voyageid@@", vessnum)

                        .Replace("@@puvendcost@@", dt.Rows[i]["puvendcost"].ToString().Replace(",", "."))
                        .Replace("@@equiprefno@@", dt.Rows[i]["equiprefno"].ToString())
                        .Replace("@@terms@@", dt.Rows[i]["terms"].ToString())

                        .Replace("@@equipmentid@@", dt.Rows[i]["equipmentid"].ToString())

                        .Replace("@@totalweight@@", dt.Rows[i]["totalweight"].ToString().Replace(",","."))
                        .Replace("@@totalqty@@", dt.Rows[i]["totalqty"].ToString().Replace(",", "."))
                        .Replace("@@totalcube@@", dt.Rows[i]["totalcube"].ToString().Replace(",", "."))
                        .Replace("@@SFcompany@@", dt.Rows[i]["SFcompany"].ToString().Replace(",", "."))
                        .Replace("@@pickuplocid@@", dt.Rows[i]["pickuplocid"].ToString())
                        .Replace("@@SFaddress@@", dt.Rows[i]["SFaddress"].ToString().Replace(Environment.NewLine," "))
                        .Replace("@@SFcity@@", dt.Rows[i]["SFcity"].ToString())
                        .Replace("@@SFstate@@", dt.Rows[i]["SFstate"].ToString())
                        .Replace("@@SFcode@@", dt.Rows[i]["SFcode"].ToString())
                        .Replace("@@STcompany@@", dt.Rows[i]["STcompany"].ToString())
                        .Replace("@@inwhlocid@@", dt.Rows[i]["inwhlocid"].ToString())
                        .Replace("@@STaddress@@", dt.Rows[i]["STaddress"].ToString().Replace(Environment.NewLine, " "))
                        .Replace("@@STcity@@", dt.Rows[i]["STcity"].ToString())
                        .Replace("@@STstate@@", dt.Rows[i]["STstate"].ToString())
                        .Replace("@@STcode@@", dt.Rows[i]["STcode"].ToString())
                        );

                    //FtpTools ft = new FtpTools(Host, Username, Password, UploadFolder, "");
                    //ft.UploadFile(filename);
                    if (File.Exists(filename))
                        sg.Execute("INSERT INTO mitcoltd.dbo.edi_exchange_tracer (recvid,status,exec_date,executed,lastValue) VALUES (" + dt.Rows[i]["recvid"].ToString() + ",'TR204',getdate(),1,'" + dt.Rows[i]["DispatchedDate"].ToString() + "')");
                }
            }
            catch (Exception)
            {

                throw;
            }

            


            return 1;
        }


    }
}
