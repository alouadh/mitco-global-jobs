﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduled.Task.Manager.Models.BJ2Miff
{
    public class TemplateXML204
    {

        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class mapping
        {            
            public mappingST ST { get; set; } = new mappingST();
            public mappingB2 B2 { get; set; } = new mappingB2();
            public mappingB2A B2A { get; set; } = new mappingB2A();
            [System.Xml.Serialization.XmlElementAttribute("L11")]
            public mappingL11[] L11 { get; set; } = new mappingL11[2];
            [System.Xml.Serialization.XmlElementAttribute("L3")]
            public mappingL3[] L3 { get; set; } = new mappingL3[2];
            public mappingPLD PLD { get; set; } = new mappingPLD();
            public mappingNTE NTE { get; set; } = new mappingNTE();
            public mappingN1 N1 { get; set; } = new mappingN1();
            public mappingN3 N3 { get; set; } = new mappingN3();
            public mappingN4 N4 { get; set; } = new mappingN4();
            public mappingG61 G61 { get; set; } = new mappingG61();
            public mappingN7loop N7loop { get; set; } = new mappingN7loop();
            [System.Xml.Serialization.XmlElementAttribute("S5loop")]
            public mappingS5loop[] S5loop { get; set; } = new mappingS5loop[2];
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingST
        {
            public string ST01 { get; set; } = "";
            public string ST02 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingB2
        {
            public string B202 { get; set; } = "";
            public string B204 { get; set; } = "";
            public string B206 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingB2A
        {
            public string B2A01 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingL11
        {
            public string L1101 { get; set; } = "";
            public string L1102 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingL3
        {
            public string L301 { get; set; } = "";
            public string L302 { get; set; } = "";
            public string L303 { get; set; } = "";
            public string L304 { get; set; } = "";
            public string L305 { get; set; } = "";
            public string L306 { get; set; } = "";
            public string L307 { get; set; } = "";
            public string L308 { get; set; } = "";
            public string L309 { get; set; } = "";
            public string L310 { get; set; } = "";
            public string L311 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingPLD
        {
            public string PLD01 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingNTE
        {
            public string NTE01 { get; set; } = "";
            public string NTE02 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingN1
        {
            public string N101 { get; set; } = "";
            public string N102 { get; set; } = "";
            public string N103 { get; set; } = "";
            public string N104 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingN3
        {
            public string N301 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingN4
        {
            public string N401 { get; set; } = "";
            public string N402 { get; set; } = "";
            public string N403 { get; set; } = "";
            public string N404 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingG61
        {
            public string G6101 { get; set; } = "";
            public string G6102 { get; set; } = "";
            public string G6103 { get; set; } = "";
            public string G6104 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingN7loop
        {
            public mappingN7loopN7 N7 { get; set; }
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingN7loopN7
        {
            public string N702 { get; set; } = "";
            public string N711 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loop
        {
            public mappingS5loopS5 S5 { get; set; } = new mappingS5loopS5();
            public mappingS5loopL11 L11 { get; set; } = new mappingS5loopL11();
            public mappingS5loopG62 G62 { get; set; } = new mappingS5loopG62();
            public mappingS5loopPLD PLD { get; set; } = new mappingS5loopPLD();
            [System.Xml.Serialization.XmlElementAttribute("NTE")]
            public mappingS5loopNTE[] NTE { get; set; } = new mappingS5loopNTE[4];
            public mappingS5loopN1 N1 { get; set; } = new mappingS5loopN1();
            public mappingS5loopN3 N3 { get; set; } = new mappingS5loopN3();
            public mappingS5loopN4 N4 { get; set; } = new mappingS5loopN4();
            public mappingS5loopG61 G61 { get; set; } = new mappingS5loopG61();
            [System.Xml.Serialization.XmlElementAttribute("OID")]
            public mappingS5loopOID[] OID { get; set; }
            public mappingS5loopL3 L3 { get; set; } = new mappingS5loopL3();
            public mappingS5loopSE SE { get; set; } = new mappingS5loopSE();
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopS5
        {
            public string S501 { get; set; } = "";
            public string S502 { get; set; } = "";
            public string S503 { get; set; } = "";
            public string S504 { get; set; } = "";
            public string S505 { get; set; } = "";
            public string S506 { get; set; } = "";
            public string S507 { get; set; } = "";
            public string S508 { get; set; } = "";
        }

        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopL11
        {
            public string L1101 { get; set; } = "";
            public string L1102 { get; set; } = "";
        }

        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopG62
        {
            public string G6201 { get; set; } = "";
            public string G6202 { get; set; } = "";
            public string G6203 { get; set; } = "";
            public string G6204 { get; set; } = "";
            public string G6205 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopPLD
        {
            public string PLD01 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopNTE
        {
            public string NTE01 { get; set; } = "";
            public string NTE02 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopN1
        {
            public string N101 { get; set; } = "";
            public string N102 { get; set; } = "";
            public string N103 { get; set; } = "";
            public string N104 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopN3
        {
            public string N301 { get; set; } = "";
            public string N302 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopN4
        {
            public string N401 { get; set; }
            public string N402 { get; set; }
            public string N403 { get; set; }
            public string N404 { get; set; }
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopG61
        {
            public string G6101 { get; set; } = "";
            public string G6102 { get; set; } = "";
            public string G6103 { get; set; } = "";
            public string G6104 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopOID
        {
            public string OID01 { get; set; } = "";
            public string OID02 { get; set; } = "";
            public string OID04 { get; set; } = "";
            public string OID05 { get; set; } = "";
            public string OID06 { get; set; } = "";
            public string OID07 { get; set; } = "";
            public string OID08 { get; set; } = "";
            public string OID09 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopL3
        {
            public string L301 { get; set; } = "";
            public string L302 { get; set; } = "";
            public string L303 { get; set; } = "";
            public string L304 { get; set; } = "";
            public string L305 { get; set; } = "";
            public string L309 { get; set; } = "";
            public string L310 { get; set; } = "";
            public string L311 { get; set; } = "";
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class mappingS5loopSE
        {
            public string SE01 { get; set; } = "";
            public string SE02 { get; set; } = "";
        }
    }
}
