﻿//using Hangfire;
//using Hangfire.LiteDB;
using System;
using System.IO;
using Hangfire;
using Hangfire.Storage.SQLite;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Topshelf;

namespace Scheduled.Task.Manager
{
    public static class Program
    {
        public static void Main()
        {

            HostFactory.Run(x =>
            {
                x.Service<Application>(s =>
                {
                    s.ConstructUsing(name => new Application());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });                
                x.RunAsLocalSystem();
                x.SetDescription("AT-Scheduled.Task.Manager#Service");
                x.SetDisplayName("#AT#Scheduled.Task.Manager");
                x.SetServiceName("AT.Scheduled.Task.Manager");
            });

        }

        public static IWebHostBuilder CreateWebHostBuilder() => WebHost.CreateDefaultBuilder().UseStartup<Startup>();



        private class Application
        {
            public void Start()
            {                
                CreateWebHostBuilder().Build().RunAsync();               
            }
            public void Stop()
            {
            }
        }


    }
}