﻿using FluentFTP;
using Scheduled.Task.Manager.Models.BJ2Miff;
using Serilog;
using Serilog.Core;
using Serilog.Sinks.LiteDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Scheduled.Task.Manager.Models.Miff2BJ
{
    class ManagerM2B
    {

        SGBD sg = new SGBD(ConfigurationManager.AppSettings["BJdb"]);

        public void SendRcv2BJ()
        {
            // Accept all orders having a container number
            AcceptOrders();
            // Fill all unrated orders having a container number with default values
            // AutoRate();

            getAccepted();
            // uploadEdiFiles();
            SendFiles2Empower();

            //getRejected();
            //uploadEdiFiles();

            getScheduled();
            // uploadEdiFiles();
            SendFiles2Empower();

            // getPickuped();
            getActual();
            // uploadEdiFiles();
            SendFiles2Empower();


            getDevanned();
            // uploadEdiFiles();
            SendFiles2Empower();


            //getActualLTL();
            //uploadEdiFiles();

        }


        // Auto Accept Shipment IDs for TL orders
        public void AcceptOrders()
        {
            // var BJlog = new LoggerConfiguration().WriteTo.LiteDB("logs/BJ/BJEDIlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJEDIlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();

            try
            {
                string sql = @"insert into  blujay_orders_status (status,acceptedBy,acceptedOn,voyageid)
(
Select 
1 status,'AUTO-ACCEPT-JOB' acceptedBy,GETDATE() acceptedOn,r.Id voyageid
from TransloadReceipt r
where (select top 1 convert(datetime,o.OrderDate) from TransloadOrder o where o.Id = r.TransloadOrderId  ) between format(getdate()-60,'yyyy-MM-dd') and getdate()
and r.CustomerId = 112
and len(r.CustomerRefNumber2) > 0
and isnumeric(r.CustomerRefNumber2) = 1 
and r.Id not in ( select es.recvid from edi_exchange_tracer es where es.recvid = r.Id and es.status = 'BJ990A')
and (select top 1 b.TenderID from blujay_message b where b.ShipmentID = r.CustomerRefNumber2 order by b.id desc) is not null
)";

                sg.Execute(sql);
            }
            catch (Exception ex)
            {
                BJlog.Error("AcceptOrders - ERROR", ex.Message);
            }



        }


        // Auto Rate Meijer orders

        public void AutoRate()
        {
            // var BJlog = new LoggerConfiguration().WriteTo.LiteDB("logs/BJ/BJEDIlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJEDIlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();
            try
            {
                string sql = @"update  receive 
	set rFuelSur	= '38'
	,rFuelCalc		= '38*1'
	,rACCDescrip	= 'DRAY+CTF'
	,rAccAmount		= '265'
	,rAccCalc		= '190+75'
	,rACCDescrip1	= 'CHASSIS'
	,rAccAmount1	= '87'
	,rAccCalc1		= '87*1'
	,rACCDescrip2	= 'PORT CONGESTION'
	,rAccAmount2	= '110'
	,rAccCalc2		= '110*1'
	,puvendcost		= '450'
	where chgdate >= GETDATE()-30
	and customerid	= 'MEIMI'
    and equiprefno <> 'N/A'
	and received	 is null
	and devandate	 is null
	and rFuelSur	 is null
	and rFuelCalc	 is null
	and rACCDescrip	 is null
	and rAccAmount	 is null
	and rAccCalc	 is null
	and rACCDescrip1 is null
	and rAccAmount1	 is null
	and rAccCalc1	 is null
	and rACCDescrip2 is null
	and rAccAmount2	 is null
	and rAccCalc2	 is null
	and puvendcost	 is null";

                sg.Execute(sql);
            }
            catch (Exception ex)
            {
                BJlog.Error("AutoRate - ERROR", ex.Message);
            }



        }

        // Accept 990 To BJ
        public string get990A()
        {
            string edibody = @"ISA*00*          *00*          *ZZ*MTDI           *ZZ*124215133      *##datenow##*##timenow##*U*00401*01       *0*P*~
GS*QM*MTDI*006903702*##fulldatenow##*##timenow##*000001*X*004010
ST*990*0001
B1*MTDI*##shipmentid##*##fulldatenow##*A
N9*TN*##tenderid##*****CN>##recvid##
SE*4*0001
GE*1*000001
IEA*1*01";
            return edibody;
        }

        // Reject 990 To BJ
        public string get990R()
        {
            string edibody = @"ISA*00*          *00*          *ZZ*MTDI           *ZZ*124215133      *##datenow##*##timenow##*U*00401*01       *0*P*~
GS*QM*MTDI*006903702*##fulldatenow##*##timenow##*000001*X*004010
ST*990*0001
B1*MTDI*##shipmentid##*##fulldatenow##*D
N9*TN*##tenderid##*****CN
K1*Notes*##notes##
SE*5*0001
GE*1*000001
IEA*1*01";
            return edibody;
        }

        // Dispatch To Trinium
        public string get204()
        {
            string edibody = @"ISA*00*          *00*          *ZZ*MITCO          *ZZ*SEAMOUNT       *200821*1538*U*00401*598017091*0*P*~
                            GS*01*MITCO*SEAMOUNT*200821*1540*598017220*X*004010
                            ST*204*0001
                            B2****271654**CC
                            B2A*00
                            AT5*TC
                            N7**AB98765432*********CN********CY**53
                            S5*1*LD*1000*K*123*PC*1001*X
                            G62*17*20200820
                            N1*SF*MITCO LIMITED*ZZ*MITAU
                            N3*2302 B ST. NW # 101
                            N4*AUBURN*WA*98001*US
                            G61*ZZ*Hassan*TE*951 357 456
                            S5*2*LD*1000*K*123*PC*1001*X
                            G62*17*20200820
                            N1*ST*MOOZ*ZZ*MOOAUB001
                            N3*8501 G ST. HN # 202
                            N4*AUBURN*WA*98005*US
                            G61*ZZ*Abbass*TE*951 357 951
                            L3*1000*A3***159****1001*X*123*K
                            SE*19*0001
                            GE*1*598017220
                            IEA*1*598017091";
            return edibody;
        }

        // Scheduled Pickup To BJ
        public string get214S()
        {
            string edibody = @"ISA*00*          *00*          *ZZ*MTDI           *ZZ*124215133      *##datenow##*##timenow##*U*00401*01       *0*P*~
GS*QM*MTDI*006903702*##fulldatenow##*##timenow##*0001*X*004010
ST*214*0001
B10*##recvid##*##shipmentid##*MTDI##po##
N1*SH*##custname##*9*##custcode##
N1*SF*##pickupname##*ZZ*##pickupaddressecode##
N3*##pickupaddresse##
N4*##pickupcity##*##pickupstate##*##pickupzipcode##*##pickupcountry##
LX*1
AT7***EP*NA*##sheduleddate##*0800
AT7***LP*NA*##sheduleddate##*1100
SE*11*0001
ST*214*0002
B10*##recvid##*##shipmentid##*MTDI##po##
L11*1*AO
N1*SH*##custname##*9*##custcode##
N1*ST*##delivryname##*ZZ*##delivryaddressecode##
N3*##delivryaddresse##
N4*##delivrycity##*##delivrystate##*##delivryzipcode##*##delivrycountry##
LX*1
AT7***ED*NA*##sheduleddate##*##sheduledtime##
AT7***LD*NA*##sheduleddate##*##sheduledtime##
SE*11*0002
GE*2*0001
IEA*1*01";
            return edibody;
        }

        // Actual Pickup To BJ
        public string get214A()
        {
            string edibody = @"ISA*00*          *00*          *ZZ*MTDI           *ZZ*124215133      *##datenow##*##timenow##*U*00401*01       *0*P*~
GS*QM*MTDI*006903702*##fulldatenow##*##timenow##*0001*X*004010
ST*214*0001
B10*##recvid##*##shipmentid##*MTDI##po##
N1*SH*##custname##*9*##custcode##
N1*SF*##pickupname##*ZZ*##pickupaddressecode##
N3*##pickupaddresse##
N4*##pickupcity##*##pickupstate##*##pickupzipcode##*##pickupcountry##
LX*1
AT7***XA*NA*##sheduleddate##*##sheduledtime##
SE*10*0001
GE*1*0001
IEA*1*01";
            return edibody;
        }



        // Confirm Pickup/Delivery To BJ
        public string get214C()
        {
            string edibody = @"ISA*00*          *00*          *ZZ*MTDI           *ZZ*124215133      *##datenow##*##timenow##*U*00401*01       *0*P*~
GS*QM*MTDI*006903702*##fulldatenow##*##timenow##*0001*X*004010
ST*214*0001
B10*##recvid##*##shipmentid##*MTDI##po##
N1*SH*##custname##*9*##custcode##
N1*SF*##pickupname##*ZZ*##pickupaddressecode##
N3*##pickupaddresse##
N4*##pickupcity##*##pickupstate##*##pickupzipcode##*##pickupcountry##
LX*1
AT7*X3*NS***##pickupdate##*0800
AT7*AF*NS***##pickupdate##*0900
SE*11*0001
ST*214*0002
B10*##recvid##*##shipmentid##*MTDI##po##
N1*SH*##custname##*9*##custcode##
N1*ST*##delivryname##*ZZ*##delivryaddressecode##
N3*##delivryaddresse##
N4*##delivrycity##*##delivrystate##*##delivryzipcode##*##delivrycountry##
LX*1
AT7*X1*NS***##devandate##*1100
AT7*CD*NS***##devandate##*1200
SE*11*0002
GE*2*0001
IEA*1*01";
            return edibody;
        }




        // Closed To BJ
        public string get210()
        {
            string edibody = @"ISA*00*          *00*          *ZZ*MTDI           *ZZ*124215133      *##datenow##*##timenow##*U*00401*01       *0*P*>
GS*QM*MTDI*006903702*##fulldatenow##*##timenow##*000001*X*004010
ST*210*0001
B3**##recvid##*##shipmentid##*CC**##fulldatenow##*##totalcharge##****MTDI
C3*USD
N9*SI*006959555
N9*MB*##masterbill##
N1*SH*MEIJER INC*9*006959555
N3*2929 WALKER AVE NW
N4*GRAND RAPIDS*MI*49544*US
S5*1*LD
G62*86*##pickupdate##*8*##pickuptime##*PS
N1*SF*##pickupname##*ZZ*##pickupaddressecode##
N3*##pickupaddress##
N4*##pickupcity##*##pickupstate##*##pickupzip##*##pickupcountry##
S5*2*UL
G62*35*##devandate##*9*##devantime##
N1*ST*##delevryname##*ZZ*##delivrycode##
N3*##delivryaddress##
N4*##delivrycity##*##delivrystate##*##delivryzipcode##*##delivrycountry##
LX*0
L1**0*FR*##totalcharge##*0*0
L3*##totalweight##*G***##totalcharge##****##totalcube##*E*##totalqty##*L
SE*21*0001
GE*1*000001
IEA*1*000001";
            return edibody;
        }

        readonly string Host = ConfigurationManager.AppSettings["BJftpHost"];
        readonly string Username = ConfigurationManager.AppSettings["BJftpUsername"];
        readonly string Password = ConfigurationManager.AppSettings["BJftpPassword"];
        readonly string UploadFolder = ConfigurationManager.AppSettings["BJUploadFolder"];







        public void getAccepted()
        {
            // var BJlog = new LoggerConfiguration().WriteTo.LiteDB("logs/BJ/BJEDIlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJEDIlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();

            try
            {
                string datenow = DateTime.Now.ToString("yyMMdd");
                string timenow = DateTime.Now.ToString("HHmm");
                string fulldatenow = DateTime.Now.ToString("yyyyMMdd");
                string editxt = get990A();
                string sql = @"select distinct
      os.voyageid recvid,r.CustomerRefNumber2 shipmentid, (select top 1 b.TenderID from blujay_message b where b.ShipmentID = r.CustomerRefNumber2 order by b.id desc) TenderID
  from 
  blujay_orders_status os, TransloadReceipt r
  where os.status = 1
  and os.voyageid not in (
      select es.recvid from edi_exchange_tracer es where es.recvid = os.voyageid and es.status = 'BJ990A'
  )
  and r.Id = os.voyageid
  and (select top 1 b.TenderID from blujay_message b where b.ShipmentID = r.CustomerRefNumber2 order by b.id desc) is not null";
                DataTable dt = sg.getTableByCommande(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string filename = "edifiles\\MTDI-990A-" + dt.Rows[i]["recvid"].ToString() + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".edi";
                    File.WriteAllText(filename, editxt
                        .Replace("##datenow##", datenow)
                        .Replace("##timenow##", timenow)
                        .Replace("##fulldatenow##", fulldatenow)
                        .Replace("##recvid##", dt.Rows[i]["recvid"].ToString())
                        .Replace("##shipmentid##", dt.Rows[i]["shipmentid"].ToString())
                        .Replace("##tenderid##", dt.Rows[i]["TenderID"].ToString())
                        );

                    //FtpTools ft = new FtpTools(Host, Username, Password, UploadFolder, "");
                    //ft.UploadFile(filename);
                    if (File.Exists(filename))
                        sg.Execute("INSERT INTO edi_exchange_tracer (recvid,status,exec_date,executed) VALUES (" + dt.Rows[i]["recvid"].ToString() + ",'BJ990A',getdate(),1)");
                }
            }
            catch (Exception ex)
            {
                BJlog.Error("getAccepted - ERROR", ex.Message);
            }
        }


        public void getRejected()
        {
            // var BJlog = new LoggerConfiguration().WriteTo.LiteDB("logs/BJ/BJEDIlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJEDIlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();
            try
            {
                string datenow = DateTime.Now.ToString("yyMMdd");
                string timenow = DateTime.Now.ToString("HHmm");
                string fulldatenow = DateTime.Now.ToString("yyyyMMdd");
                string editxt = get990R();
                string sql = @"select distinct
      os.voyageid recvid,r.custrefno2 shipmentid,os.rejectedNotes notes, (select b.TenderID from BlujayEDI.dbo.blujay_message b where b.ShipmentID = r.custrefno2 order by b.id desc) TenderID
  from 
   blujay_orders_status os,  receive r
  where os.status = -1
  and format(os.rejectedOn,'yyyyMMdd') = format(getdate(),'yyyyMMdd')
  and os.voyageid not in (
      select es.recvid from  edi_exchange_tracer es where es.recvid = os.voyageid and es.status = 'BJ990R'
  )
  and r.recvid = os.voyageid";

                DataTable dt = sg.getTableByCommande(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string filename = "edifiles\\MTDI-990R-" + dt.Rows[i]["recvid"].ToString() + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".edi";
                    File.WriteAllText(filename, editxt
                        .Replace("##datenow##", datenow)
                        .Replace("##timenow##", timenow)
                        .Replace("##fulldatenow##", fulldatenow)
                        .Replace("##tenderid##", dt.Rows[i]["TenderID"].ToString())
                        .Replace("##shipmentid##", dt.Rows[i]["shipmentid"].ToString())
                        .Replace("##notes##", dt.Rows[i]["notes"].ToString())
                        );

                    //FtpTools ft = new FtpTools(Host, Username, Password, UploadFolder, "");
                    //ft.UploadFile(filename);

                    sg.Execute("INSERT INTO  edi_exchange_tracer (recvid,status,exec_date,executed) VALUES (" + dt.Rows[i]["recvid"].ToString() + ",'BJ990R',getdate(),1)");
                }
            }
            catch (Exception ex)
            {
                BJlog.Error("getRejected - ERROR", ex.Message);
            }


        }


        public DataTable getDispatched()
        {
            string sql = @"select distinct
      os.voyageid recvid
  from 
   blujay_orders_status os
  where os.status = 2
  and format(os.status_date,'yyyyMMdd') = format(getdate(),'yyyyMMdd')
  and os.voyageid not in (
      select es.recvid from  edi_exchange_tracer es where es.recvid = os.voyageid and es.status = 'TR204'
  )";

            return sg.getTableByCommande(sql);

        }



        public void getScheduled()
        {
            // var BJlog = new LoggerConfiguration().WriteTo.LiteDB("logs/BJ/BJEDIlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJEDIlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();
            try
            {
                string datenow = DateTime.Now.ToString("yyMMdd");
                string timenow = DateTime.Now.ToString("HHmm");
                string fulldatenow = DateTime.Now.ToString("yyyyMMdd");
                string editxt = get214S();
                string sql = @"SELECT DISTINCT
      r.Id recvid,
      r.CustomerRefNumber2 shipmentid,
  	isnull(r.MasterBill,'N/A') masterbill,
  	format(convert(datetime,r.DispatchDate),'yyyyMMdd') pickupdate,
  	'0800' pickuptime,
      format(convert(datetime,r.received),'yyyyMMdd') devandate,
      '1100' devantime,
      b.CustomerName custname, 
      b.Customerrefenrence custcode, 
      b.PickupLocationName pickupname, 
      b.PickupLocationAddressCode pickupaddressecode, 
      b.PickupLocationShortAddress pickupaddress, 
      b.DeliveryLocationName delivryname, 
      b.PickupLocationCity pickupcity, 
      b.DeliveryLocationCity delivrycity, 
      b.PickupLocationState pickupstate, 
      b.PickupLocationZipCode pickupzip,
      b.PickupLocationCountry pickupcountry, 
      b.DeliveryLocationAddressCode delivryaddressecode, 
      b.DeliveryLocationShortAddress delivryaddresse, 
      b.DeliveryLocationState delivrystate, 
      b.DeliveryLocationZipCode delivryzipcode, 
      b.DeliveryLocationCountry delivrycountry,
  	isnull(convert(decimal(10,2),round((select sum(ri.weight) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%')*2.205,2)),0) totalweight,
  		isnull((select 
		format(sum(isnull(rat.Rate,0) * isnull(rat.Quantity,0)-isnull(rat2.FreeTime,0)),'0.00') rating
	from	InboundOutboundRate rat,Rating rat2
	where	rat.RatingId = rat2.Id
	and		rat.TransloadReceiptId = r.Id 
	and		rat.Enabled            = 1),0) totalcharge,
  	isnull(convert(decimal(10,2),round((select sum(ri.cube) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%')*35.315,2)),0) totalcube,
  	isnull((select sum(ri.UnitCount) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%'),0) totalqty,
    (SELECT distinct ',L11*' + rtrim(ri.PurchaseOrder) + '*OQ' FROM   TransloadReceiptItem ri WHERE ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%' FOR XML PATH(''))  pono 
  from  TransloadReceipt r,blujay_message b
  where b.ShipmentID = r.CustomerRefNumber2
  and r.CustomerId = 112
  and r.DispatchDate is not null
  and ISDATE(r.DispatchDate) = 1
  and (select top 1 convert(datetime,o.OrderDate) from  TransloadOrder o where o.Id = r.TransloadOrderId ) between format(getdate()-60,'yyyy-MM-dd') and getdate()
  and r.Id not in (
      select es.recvid from  edi_exchange_tracer es where es.recvid = r.Id and es.status = 'BJ214A'
  )
  and isnull(r.IsCanceled,'0') <> '1' ";

                DataTable dt = sg.getTableByCommande(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string filename = "edifiles\\MTDI-214S-" + dt.Rows[i]["recvid"].ToString() + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".edi";

                    File.WriteAllText(filename, editxt
                        .Replace("##datenow##", datenow)
                        .Replace("##timenow##", timenow)
                        .Replace("##fulldatenow##", fulldatenow)
                        .Replace("##recvid##", dt.Rows[i]["recvid"].ToString())
                        .Replace("##shipmentid##", dt.Rows[i]["shipmentid"].ToString())
                        .Replace("##custname##", dt.Rows[i]["custname"].ToString())
                        .Replace("##custcode##", dt.Rows[i]["custcode"].ToString())
                        .Replace("##pickupname##", dt.Rows[i]["pickupname"].ToString())
                        .Replace("##pickupaddressecode##", dt.Rows[i]["pickupaddressecode"].ToString())
                        .Replace("##pickupaddresse##", dt.Rows[i]["pickupaddresse"].ToString())
                        .Replace("##pickupzipcode##", dt.Rows[i]["pickupzipcode"].ToString())
                        .Replace("##pickupstate##", dt.Rows[i]["pickupstate"].ToString())
                        .Replace("##pickupcountry##", dt.Rows[i]["pickupcountry"].ToString())
                        .Replace("##sheduleddate##", dt.Rows[i]["sheduleddate"].ToString())
                        .Replace("##sheduledtime##", dt.Rows[i]["sheduledtime"].ToString())
                        .Replace("##pickupcity##", dt.Rows[i]["pickupcity"].ToString())
                        .Replace("##po##", dt.Rows[i]["pono"].ToString().Replace(",", Environment.NewLine))
                        .Replace("##delivryname##", dt.Rows[i]["delivryname"].ToString())
                        .Replace("##delivryaddressecode##", dt.Rows[i]["delivryaddressecode"].ToString())
                        .Replace("##delivryaddresse##", dt.Rows[i]["delivryaddresse"].ToString())
                        .Replace("##delivryzipcode##", dt.Rows[i]["delivryzipcode"].ToString())
                        .Replace("##delivrystate##", dt.Rows[i]["delivrystate"].ToString())
                        .Replace("##delivrycountry##", dt.Rows[i]["delivrycountry"].ToString())
                        .Replace("##delivrycity##", dt.Rows[i]["delivrycity"].ToString())
                        );
                    if (File.Exists(filename))
                        sg.Execute("INSERT INTO  edi_exchange_tracer (recvid,status,exec_date,executed) VALUES (" + dt.Rows[i]["recvid"].ToString() + ",'BJ214S',getdate(),1)");
                }
            }
            catch (Exception ex)
            {
                BJlog.Error("getScheduled - ERROR" + ex.Message);
            }



        }

        public void getPickuped()
        {
            // var BJlog = new LoggerConfiguration().WriteTo.LiteDB("logs/BJ/BJEDIlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJEDIlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();
            try
            {
                string datenow = DateTime.Now.ToString("yyMMdd");
                string timenow = DateTime.Now.ToString("HHmm");
                string fulldatenow = DateTime.Now.ToString("yyyyMMdd");
                //string editxt = get214A();
                string editxt = get214C();

                string sql = @"select distinct
      r.Id recvid,
      r.CustomerRefNumber2 shipmentid,
  	isnull(r.masterbill,'N/A') masterbill,
  	format(convert(datetime,r.PickupDate),'yyyyMMdd') pickupdate,
  	'0800' pickuptime,
      format(convert(datetime,r.received),'yyyyMMdd') devandate,
      '1100' devantime,
      b.CustomerName custname, 
      b.Customerrefenrence custcode, 
      b.PickupLocationName pickupname, 
      b.PickupLocationAddressCode pickupaddressecode, 
      b.PickupLocationShortAddress pickupaddress, 
      b.DeliveryLocationName delivryname, 
      b.PickupLocationCity pickupcity, 
      b.DeliveryLocationCity delivrycity, 
      b.PickupLocationState pickupstate, 
      b.PickupLocationZipCode pickupzip,
      b.PickupLocationCountry pickupcountry, 
      b.DeliveryLocationAddressCode delivryaddressecode, 
      b.DeliveryLocationShortAddress delivryaddresse, 
      b.DeliveryLocationState delivrystate, 
      b.DeliveryLocationZipCode delivryzipcode, 
      b.DeliveryLocationCountry delivrycountry,
  	isnull(convert(decimal(10,2),round((select sum(ri.weight) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%')*2.205,2)),0) totalweight,
  		isnull((select 
		format(sum(isnull(rat.Rate,0) * isnull(rat.Quantity,0)-isnull(rat2.FreeTime,0)),'0.00') rating
	from	InboundOutboundRate rat,Rating rat2
	where	rat.RatingId = rat2.Id
	and		rat.TransloadReceiptId = r.Id 
	and		rat.Enabled            = 1),0) totalcharge,
  	isnull(convert(decimal(10,2),round((select sum(ri.cube) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%')*35.315,2)),0) totalcube,
  	isnull((select sum(ri.UnitCount) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%'),0) totalqty,
    (SELECT distinct ',L11*' + rtrim(ri.PurchaseOrder) + '*OQ' FROM   TransloadReceiptItem ri WHERE ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%' FOR XML PATH(''))  pono 
  from  TransloadReceipt r, blujay_message b
  where b.ShipmentID = r.CustomerRefNumber2
  and r.CustomerId = 112
  and r.PickupDate is not null
  and ISDATE(r.PickupDate) = 1
  and (select top 1 convert(datetime,o.OrderDate) from  TransloadOrder o where o.Id = r.TransloadOrderId ) between format(getdate()-60,'yyyy-MM-dd') and getdate()
  and r.Id not in (
      select es.recvid from  edi_exchange_tracer es where es.recvid = r.Id and es.status = 'BJ214A'
  )
  and isnull(r.IsCanceled,'0') <> '1' ";

                DataTable dt = sg.getTableByCommande(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string filename = "edifiles\\MTDI-214A-" + dt.Rows[i]["recvid"].ToString() + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".edi";

                    File.WriteAllText(filename, editxt
                        .Replace("##datenow##", datenow)
                        .Replace("##timenow##", timenow)
                        .Replace("##fulldatenow##", fulldatenow)
                        .Replace("##recvid##", dt.Rows[i]["recvid"].ToString())
                        .Replace("##shipmentid##", dt.Rows[i]["shipmentid"].ToString())
                        .Replace("##custname##", dt.Rows[i]["custname"].ToString())
                        .Replace("##custcode##", dt.Rows[i]["custcode"].ToString())
                        .Replace("##pickupname##", dt.Rows[i]["pickupname"].ToString())
                        .Replace("##pickupaddressecode##", dt.Rows[i]["pickupaddressecode"].ToString())
                        .Replace("##pickupaddresse##", dt.Rows[i]["pickupaddress"].ToString())
                        .Replace("##pickupzipcode##", dt.Rows[i]["pickupzip"].ToString())
                        .Replace("##pickupstate##", dt.Rows[i]["pickupstate"].ToString())
                        .Replace("##pickupcountry##", dt.Rows[i]["pickupcountry"].ToString())
                        .Replace("##pickupdate##", dt.Rows[i]["pickupdate"].ToString())
                        .Replace("##devandate##", dt.Rows[i]["devandate"].ToString())
                        .Replace("##pickupcity##", dt.Rows[i]["pickupcity"].ToString())
                        .Replace("##po##", dt.Rows[i]["pono"].ToString().Replace(",", Environment.NewLine))
                        .Replace("##delivryname##", dt.Rows[i]["delivryname"].ToString())
                        .Replace("##delivryaddressecode##", dt.Rows[i]["delivryaddressecode"].ToString())
                        .Replace("##delivryaddresse##", dt.Rows[i]["delivryaddresse"].ToString())
                        .Replace("##delivryzipcode##", dt.Rows[i]["delivryzipcode"].ToString())
                        .Replace("##delivrystate##", dt.Rows[i]["delivrystate"].ToString())
                        .Replace("##delivrycountry##", dt.Rows[i]["delivrycountry"].ToString())
                        .Replace("##delivrycity##", dt.Rows[i]["delivrycity"].ToString())
                        );
                    if (File.Exists(filename))
                        sg.Execute("INSERT INTO  edi_exchange_tracer (recvid,status,exec_date,executed) VALUES (" + dt.Rows[i]["recvid"].ToString() + ",'BJ214A',getdate(),1)");
                }
            }
            catch (Exception ex)
            {
                BJlog.Error("getActualPickup - ERROR", ex.Message);
            }

        }


        public void getDevanned()
        {
            // var BJlog = new LoggerConfiguration().WriteTo.LiteDB("logs/BJ/BJEDIlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJEDIlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();

            try
            {
                string datenow = DateTime.Now.ToString("yyMMdd");
                string timenow = DateTime.Now.ToString("HHmm");
                string fulldatenow = DateTime.Now.ToString("yyyyMMdd");

                string sql = @"select
      r.Id recvid,
      r.CustomerRefNumber2 shipmentid,
  	isnull(r.masterbill,'N/A') masterbill,
  	isnull(format(convert(datetime,r.DispatchDate),'yyyyMMdd'),format(convert(datetime,r.Received),'yyyyMMdd')) pickupdate,
  	'0800' pickuptime,
      format(convert(datetime,r.Received),'yyyyMMdd') devandate,
      '1100' devantime,
      b.CustomerName custname, 
      b.Customerrefenrence custcode, 
      b.PickupLocationName pickupname, 
      b.PickupLocationAddressCode pickupaddressecode, 
      b.PickupLocationShortAddress pickupaddress, 
      b.DeliveryLocationName delivryname, 
      b.PickupLocationCity pickupcity, 
      b.DeliveryLocationCity delivrycity, 
      b.PickupLocationState pickupstate, 
      b.PickupLocationZipCode pickupzip,
      b.PickupLocationCountry pickupcountry, 
      b.DeliveryLocationAddressCode delivryaddressecode, 
      b.DeliveryLocationShortAddress delivryaddresse, 
      b.DeliveryLocationState delivrystate, 
      b.DeliveryLocationZipCode delivryzipcode, 
      b.DeliveryLocationCountry delivrycountry,
  	isnull(convert(decimal(10,2),round((select sum(ri.weight) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%')*2.205,2)),0) totalweight,
  		isnull((select 
		format(sum(isnull(rat.Rate,0) * isnull(rat.Quantity,0)-isnull(rat2.FreeTime,0)),'0.00') rating
	from	InboundOutboundRate rat,Rating rat2
	where	rat.RatingId = rat2.Id
	and		rat.TransloadReceiptId = r.Id 
	and		rat.Enabled            = 1),0) totalcharge,
  	isnull(convert(decimal(10,2),round((select sum(ri.cube) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%')*35.315,2)),0) totalcube,
  	isnull((select sum(ri.UnitCount) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%'),0) totalqty,
    (SELECT distinct ',L11*' + rtrim(ri.PurchaseOrder) + '*OQ' FROM   TransloadReceiptItem ri WHERE ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%' FOR XML PATH(''))  pono 
  from 
   TransloadReceipt r, blujay_message b
  where r.CustomerId = 112
  and b.ShipmentID = rtrim(r.CustomerRefNumber2)
  and FORMAT((select top 1 convert(decimal(12,2),isnull(replace(bb.charge,',',''),0)) from  blujay_message bb where bb.ShipmentID = r.CustomerRefNumber2 order by id desc)/100,'0.00')
  =
  isnull((select 
		format(sum(isnull(rat.Rate,0) * isnull(rat.Quantity,0)-isnull(rat2.FreeTime,0)),'0.00') rating
	from	InboundOutboundRate rat,Rating rat2
	where	rat.RatingId = rat2.Id
	and		rat.TransloadReceiptId = r.Id 
	and		rat.Enabled            = 1),0) 
  and (select top 1 convert(datetime,o.OrderDate) from  TransloadOrder o where o.Id = r.TransloadOrderId ) between format(getdate()-180,'yyyy-MM-dd') and getdate()
  and r.StatusId = 23
  and isnull(r.IsCanceled,'') <> '1'
  and b.Id = (select top 1 bm.Id from  blujay_message bm where bm.ShipmentID = b.ShipmentID order by bm.Id desc)
  and r.Id not in (
      select es.recvid from  edi_exchange_tracer es where es.recvid = r.Id and es.status = 'BJ210'
  ) ";

                DataTable dt = sg.getTableByCommande(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    string editxt = get210();
                    string filename = "edifiles\\MTDI-210-" + dt.Rows[i]["recvid"].ToString() + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".edi";
                    File.WriteAllText(filename, editxt
                        .Replace("##datenow##", datenow)
                        .Replace("##timenow##", timenow)
                        .Replace("##fulldatenow##", fulldatenow)
                        .Replace("##recvid##", dt.Rows[i]["recvid"].ToString())
                        .Replace("##shipmentid##", dt.Rows[i]["shipmentid"].ToString())
                        .Replace("##pickupname##", dt.Rows[i]["pickupname"].ToString())
                        .Replace("##pickupaddressecode##", dt.Rows[i]["pickupaddressecode"].ToString())
                        .Replace("##pickupaddress##", dt.Rows[i]["pickupaddress"].ToString())
                        .Replace("##pickupstate##", dt.Rows[i]["pickupstate"].ToString())
                        .Replace("##pickupcity##", dt.Rows[i]["pickupcity"].ToString())
                        .Replace("##pickupzip##", dt.Rows[i]["pickupzip"].ToString())
                        .Replace("##pickupcountry##", dt.Rows[i]["pickupcountry"].ToString())
                        .Replace("##delevryname##", dt.Rows[i]["delivryname"].ToString())
                        .Replace("##delivrycode##", dt.Rows[i]["delivryaddressecode"].ToString())
                        .Replace("##delivryaddress##", dt.Rows[i]["delivryaddresse"].ToString())
                        .Replace("##delivryzipcode##", dt.Rows[i]["delivryzipcode"].ToString())
                        .Replace("##delivrystate##", dt.Rows[i]["delivrystate"].ToString())
                        .Replace("##delivrycountry##", dt.Rows[i]["delivrycountry"].ToString())
                        .Replace("##pickupdate##", dt.Rows[i]["pickupdate"].ToString())
                        .Replace("##pickuptime##", dt.Rows[i]["pickuptime"].ToString())
                        .Replace("##devandate##", dt.Rows[i]["devandate"].ToString())
                        .Replace("##devantime##", dt.Rows[i]["devantime"].ToString())
                        .Replace("##delivrycity##", dt.Rows[i]["delivrycity"].ToString())
                        .Replace("##totalweight##", dt.Rows[i]["totalweight"].ToString().Replace(",", "."))
                        .Replace("##totalcharge##", dt.Rows[i]["totalcharge"].ToString())
                        .Replace("##totalcube##", dt.Rows[i]["totalcube"].ToString().Replace(",", "."))
                        .Replace("##totalqty##", dt.Rows[i]["totalqty"].ToString())
                        .Replace("##masterbill##", dt.Rows[i]["masterbill"].ToString())
                        );
                    if (File.Exists(filename))
                        sg.Execute("INSERT INTO  edi_exchange_tracer (recvid,status,exec_date,executed) VALUES (" + dt.Rows[i]["recvid"].ToString() + ",'BJ210',getdate(),1)");
                }
            }
            catch (Exception ex)
            {
                BJlog.Error("getDevaned - ERROR", ex.Message);
            }



        }

        public void getActual()
        {
            // var BJlog = new LoggerConfiguration().WriteTo.LiteDB("logs/BJ/BJEDIlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJEDIlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();

            try
            {
                string datenow = DateTime.Now.ToString("yyMMdd");
                string timenow = DateTime.Now.ToString("HHmm");
                string fulldatenow = DateTime.Now.ToString("yyyyMMdd");
                string editxt = get214C();

                string sql = @"select
      r.Id recvid,
      r.CustomerRefNumber2 shipmentid,
  	isnull(r.masterbill,'N/A') masterbill,
  	format(convert(datetime,r.PickupDate),'yyyyMMdd') pickupdate,
  	'0800' pickuptime,
      format(convert(datetime,r.DevanDate),'yyyyMMdd') devandate,
      '1100' devantime,
      b.CustomerName custname, 
      b.Customerrefenrence custcode, 
      b.PickupLocationName pickupname, 
      b.PickupLocationAddressCode pickupaddressecode, 
      b.PickupLocationShortAddress pickupaddress, 
      b.DeliveryLocationName delivryname, 
      b.PickupLocationCity pickupcity, 
      b.DeliveryLocationCity delivrycity, 
      b.PickupLocationState pickupstate, 
      b.PickupLocationZipCode pickupzip,
      b.PickupLocationCountry pickupcountry, 
      b.DeliveryLocationAddressCode delivryaddressecode, 
      b.DeliveryLocationShortAddress delivryaddresse, 
      b.DeliveryLocationState delivrystate, 
      b.DeliveryLocationZipCode delivryzipcode, 
      b.DeliveryLocationCountry delivrycountry,
  	isnull(convert(decimal(10,2),round((select sum(ri.weight) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%')*2.205,2)),0) totalweight,
  		isnull((select 
		format(sum(isnull(rat.Rate,0) * isnull(rat.Quantity,0)-isnull(rat2.FreeTime,0)),'0.00') rating
	from	InboundOutboundRate rat,Rating rat2
	where	rat.RatingId = rat2.Id
	and		rat.TransloadReceiptId = r.Id 
	and		rat.Enabled            = 1),0) totalcharge,
  	isnull(convert(decimal(10,2),round((select sum(ri.cube) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%')*35.315,2)),0) totalcube,
  	isnull((select sum(ri.UnitCount) from  TransloadReceiptItem ri where ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%'),0) totalqty,
    (SELECT distinct ',L11*' + rtrim(ri.PurchaseOrder) + '*OQ' FROM   TransloadReceiptItem ri WHERE ri.TransloadReceiptId = r.Id and ri.Description not like '%CANCEL%' FOR XML PATH(''))  pono 
  from 
   TransloadReceipt r, blujay_message b
  where r.CustomerId = 112
  and b.ShipmentID = rtrim(r.CustomerRefNumber2)
  and FORMAT((select top 1 convert(decimal(12,2),isnull(replace(bb.charge,',',''),0)) from  blujay_message bb where bb.ShipmentID = r.CustomerRefNumber2 order by id desc)/100,'0.00')
  =
  isnull((select 
		format(sum(isnull(rat.Rate,0) * isnull(rat.Quantity,0)-isnull(rat2.FreeTime,0)),'0.00') rating
	from	InboundOutboundRate rat,Rating rat2
	where	rat.RatingId = rat2.Id
	and		rat.TransloadReceiptId = r.Id 
	and		rat.Enabled            = 1),0) 
  and (select top 1 convert(datetime,o.OrderDate) from  TransloadOrder o where o.Id = r.TransloadOrderId ) between format(getdate()-180,'yyyy-MM-dd') and getdate()  
  and r.StatusId = 23
  and isnull(r.IsCanceled,'') <> '1'
  and b.Id = (select top 1 bm.Id from  blujay_message bm where bm.ShipmentID = b.ShipmentID order by bm.Id desc)
  and r.Id not in (
      select es.recvid from  edi_exchange_tracer es where es.recvid = r.Id and es.status = 'BJ214C'
  )  ";

                DataTable dt = sg.getTableByCommande(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // L11 * 1 * AO

                    string filename = "edifiles\\MTDI-214C-" + dt.Rows[i]["recvid"].ToString() + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".edi";
                    File.WriteAllText(filename, editxt
                        .Replace("##datenow##", datenow)
                        .Replace("##timenow##", timenow)
                        .Replace("##fulldatenow##", fulldatenow)
                        .Replace("##recvid##", dt.Rows[i]["recvid"].ToString())
                        .Replace("##shipmentid##", dt.Rows[i]["shipmentid"].ToString())
                        .Replace("##custname##", dt.Rows[i]["custname"].ToString())
                        .Replace("##custcode##", dt.Rows[i]["custcode"].ToString())
                        .Replace("##pickupname##", dt.Rows[i]["pickupname"].ToString())
                        .Replace("##pickupaddressecode##", dt.Rows[i]["pickupaddressecode"].ToString())
                        .Replace("##pickupaddresse##", dt.Rows[i]["pickupaddress"].ToString())
                        .Replace("##pickupzipcode##", dt.Rows[i]["pickupzip"].ToString())
                        .Replace("##pickupstate##", dt.Rows[i]["pickupstate"].ToString())
                        .Replace("##pickupcountry##", dt.Rows[i]["pickupcountry"].ToString())
                        .Replace("##pickupdate##", dt.Rows[i]["pickupdate"].ToString())
                        .Replace("##devandate##", dt.Rows[i]["devandate"].ToString())
                        .Replace("##pickupcity##", dt.Rows[i]["pickupcity"].ToString())
                        .Replace("##po##", dt.Rows[i]["pono"].ToString().Replace(",", Environment.NewLine) + Environment.NewLine + "L11*1*AO")
                        .Replace("##delivryname##", dt.Rows[i]["delivryname"].ToString())
                        .Replace("##delivryaddressecode##", dt.Rows[i]["delivryaddressecode"].ToString())
                        .Replace("##delivryaddresse##", dt.Rows[i]["delivryaddresse"].ToString())
                        .Replace("##delivryzipcode##", dt.Rows[i]["delivryzipcode"].ToString())
                        .Replace("##delivrystate##", dt.Rows[i]["delivrystate"].ToString())
                        .Replace("##delivrycountry##", dt.Rows[i]["delivrycountry"].ToString())
                        .Replace("##delivrycity##", dt.Rows[i]["delivrycity"].ToString())
                        );
                    if (File.Exists(filename))
                        sg.Execute("INSERT INTO  edi_exchange_tracer (recvid,status,exec_date,executed) VALUES (" + dt.Rows[i]["recvid"].ToString() + ",'BJ214C',getdate(),1)");
                }
            }
            catch (Exception ex)
            {
                BJlog.Error("getActual - ERROR", ex.Message);
            }
        }

        public void getActualLTL()
        {
            // var BJlog = new LoggerConfiguration().WriteTo.LiteDB("logs/BJ/BJEDIlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJ/BJEDIlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();
            try
            {
                string datenow = DateTime.Now.ToString("yyMMdd");
                string timenow = DateTime.Now.ToString("HHmm");
                string fulldatenow = DateTime.Now.ToString("yyyyMMdd");
                string editxt = get214C();

                string sql = @"select
      r.recvid recvid,
      r.custrefno2 shipmentid,
  	isnull(r.masterbill,'N/A') masterbill,
  	isnull(format(convert(datetime,r.DispatchedDate),'yyyyMMdd'),format(convert(datetime,r.received),'yyyyMMdd')) pickupdate,
  	'0800' pickuptime,
      format(convert(datetime,r.received),'yyyyMMdd') devandate,
      '1100' devantime,
      b.CustomerName custname, 
      b.Customerrefenrence custcode, 
      b.PickupLocationName pickupname, 
      b.PickupLocationAddressCode pickupaddressecode, 
      b.PickupLocationShortAddress pickupaddress, 
      b.DeliveryLocationName delivryname, 
      b.PickupLocationCity pickupcity, 
      b.DeliveryLocationCity delivrycity, 
      b.PickupLocationState pickupstate, 
      b.PickupLocationZipCode pickupzip,
      b.PickupLocationCountry pickupcountry, 
      b.DeliveryLocationAddressCode delivryaddressecode, 
      b.DeliveryLocationShortAddress delivryaddresse, 
      b.DeliveryLocationState delivrystate, 
      b.DeliveryLocationZipCode delivryzipcode, 
      b.DeliveryLocationCountry delivrycountry,
  	convert(decimal(10,2),round((select sum(ri.weight) from  recvitems ri where ri.recvid = r.recvid and ri.descrip not like '%CANCEL%')*2.205,2)) totalweight,
  	convert(float,(IsNull(r.rbillrate, 0)+IsNull(r.rFuelSur, 0)+IsNull(r.rAccAmount, 0)+IsNull(r.rAccAmount1, 0)+IsNull(r.rAccAmount2, 0)))*100 totalcharge,
  	convert(decimal(10,2),round((select sum(ri.cube) from  recvitems ri where ri.recvid = r.recvid and ri.descrip not like '%CANCEL%')*35.315,2)) totalcube,
  	(select sum(ri.packqty) from  recvitems ri where ri.recvid = r.recvid and ri.descrip not like '%CANCEL%') totalqty,
      (SELECT distinct ',L11*' + ri.pono + '*OQ' FROM   recvitems ri WHERE ri.recvid = r.recvid and ri.descrip not like '%CANCEL%' FOR XML PATH(''))  pono 
  from 
   receive r,BlujayEDI.dbo.blujay_message b
  where r.customerid = 'MEIMI'
  and r.ShippingTerms = 'LTL'
  and b.ShipmentID = rtrim(r.custrefno2)
  and round((select top 1 convert(decimal(12,2),isnull(replace(bb.charge,',',''),0)) from blujayedi.dbo.blujay_message bb where bb.ShipmentID = r.custrefno2 order by id desc)/100,0)
  =
  round(IsNull(r.rbillrate, 0)+IsNull(r.rFuelSur, 0)+IsNull(r.rAccAmount, 0)+IsNull(r.rAccAmount1, 0)+IsNull(r.rAccAmount2, 0),0)
  and  convert(datetime,r.om_order_date) >= getdate()-180
  and  r.om_order_date is not null
  and r.om_status = 'Closed'
  and isnull(r.cancelflag,'') <> '1'
  and b.Id = (select top 1 bm.Id from BlujayEDI.dbo.blujay_message bm where bm.ShipmentID = b.ShipmentID order by bm.Id desc)
  and r.recvid not in (
      select es.recvid from  edi_exchange_tracer es where es.recvid = r.recvid and es.status = 'BJ214C2'
  )";
                /*
  and r.recvid in (
     select voyageid from  blujay_orders_status where status = 6 and devannedBy = 'OM210' and voyageid = r.recvid
  )";

    */
                DataTable dt = sg.getTableByCommande(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // L11 * 1 * AO

                    string filename = "edifiles\\MTDI-214C2-" + dt.Rows[i]["recvid"].ToString() + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".edi";
                    File.WriteAllText(filename, editxt
                        .Replace("##datenow##", datenow)
                        .Replace("##timenow##", timenow)
                        .Replace("##fulldatenow##", fulldatenow)
                        .Replace("##recvid##", dt.Rows[i]["recvid"].ToString())
                        .Replace("##shipmentid##", dt.Rows[i]["shipmentid"].ToString())
                        .Replace("##custname##", dt.Rows[i]["custname"].ToString())
                        .Replace("##custcode##", dt.Rows[i]["custcode"].ToString())
                        .Replace("##pickupname##", dt.Rows[i]["pickupname"].ToString())
                        .Replace("##pickupaddressecode##", dt.Rows[i]["pickupaddressecode"].ToString())
                        .Replace("##pickupaddresse##", dt.Rows[i]["pickupaddress"].ToString())
                        .Replace("##pickupzipcode##", dt.Rows[i]["pickupzip"].ToString())
                        .Replace("##pickupstate##", dt.Rows[i]["pickupstate"].ToString())
                        .Replace("##pickupcountry##", dt.Rows[i]["pickupcountry"].ToString())
                        .Replace("##pickupdate##", dt.Rows[i]["pickupdate"].ToString())
                        .Replace("##devandate##", dt.Rows[i]["devandate"].ToString())
                        .Replace("##pickupcity##", dt.Rows[i]["pickupcity"].ToString())
                        .Replace("##po##", dt.Rows[i]["pono"].ToString().Replace(",", Environment.NewLine) + Environment.NewLine + "L11*1*AO")
                        .Replace("##delivryname##", dt.Rows[i]["delivryname"].ToString())
                        .Replace("##delivryaddressecode##", dt.Rows[i]["delivryaddressecode"].ToString())
                        .Replace("##delivryaddresse##", dt.Rows[i]["delivryaddresse"].ToString())
                        .Replace("##delivryzipcode##", dt.Rows[i]["delivryzipcode"].ToString())
                        .Replace("##delivrystate##", dt.Rows[i]["delivrystate"].ToString())
                        .Replace("##delivrycountry##", dt.Rows[i]["delivrycountry"].ToString())
                        .Replace("##delivrycity##", dt.Rows[i]["delivrycity"].ToString())
                        );
                    if (File.Exists(filename))
                        sg.Execute("INSERT INTO  edi_exchange_tracer (recvid,status,exec_date,executed) VALUES (" + dt.Rows[i]["recvid"].ToString() + ",'BJ214C2',getdate(),1)");
                }
            }
            catch (Exception ex)
            {
                BJlog.Error("getActualLTL - ERROR", ex.Message);
            }
        }


        readonly string Host_ = ConfigurationManager.AppSettings["EMPOWERftpHost"];
        readonly string Username_ = ConfigurationManager.AppSettings["EMPOWERftpUsername"];
        readonly string Password_ = ConfigurationManager.AppSettings["EMPOWERftpPassword"];
        readonly string UploadFolder_ = ConfigurationManager.AppSettings["EMPOWERSendFolder"];

        private bool CheckIfFileExistsOnServer(string fileName)
        {
            var request = (FtpWebRequest)WebRequest.Create("ftp://" + Host + "/" + UploadFolder + "/" + fileName);
            request.Credentials = new NetworkCredential(Username, Password);
            request.Method = WebRequestMethods.Ftp.GetFileSize;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    return false;
            }
            return false;
        }

        public int SendFiles2Empower()
        {
            var BJlog = new LoggerConfiguration().WriteTo.File("logs/BJEMP/BJEMPlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();


            // FtpTools ft = new FtpTools(Host, Username, Password, UploadFolder, "");

            FtpClient client = new FtpClient(Host_);
            client.Credentials = new NetworkCredential(Username_, Password_);
            client.Connect();


            DirectoryInfo info = new DirectoryInfo("edifiles");
            FileInfo[] files = info.GetFiles().Where(p => p.CreationTime.ToString("yyyyMMdd") == DateTime.Now.AddDays(0).ToString("yyyyMMdd")).OrderBy(p => p.CreationTime).ToArray();

            try
            {
                foreach (var filename in files)
                {

                    // client.Upload(); // (ToFolder + "/" + item.Name, item.FullName);
                    
                    var up = client.UploadFile(filename.FullName, UploadFolder_ + "/" + filename.Name);
                    if(up == FtpStatus.Success)
                    {
                        File.Move(filename.FullName, "edifiles\\Processed\\" + filename.Name, true);
                    }
                    
                    //using (var client = new WebClient())
                    //{
                    //    if (!CheckIfFileExistsOnServer(filename.Name))
                    //    {
                    //        client.Credentials = new NetworkCredential(Username_, Password_);
                    //        var up = client.UploadFile("ftp://" + Host_ + "/" + UploadFolder_ + "/" + filename.Name, WebRequestMethods.Ftp.UploadFile, filename.FullName);
                    //        File.Move(filename.FullName, "edifiles\\Processed\\" + filename.Name, true);
                    //    }
                    //}

                }
            }
            catch (Exception ex)
            {
                BJlog.Error(ex.Message);
            }

            return 1;
        }


        public void uploadEdiFiles()
        {

            FtpTools ft = new FtpTools(Host, Username, Password, UploadFolder, "");
            DirectoryInfo info = new DirectoryInfo("edifiles");
            FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).ToArray();

            foreach (var filename in files)
            {
                if (ft.UploadFile(filename.FullName))
                {
                    File.Move(filename.FullName, "edifiles\\Processed\\" + filename.Name, true);
                }
            }
        }

    }
}
