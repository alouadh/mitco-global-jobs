﻿using EdiTools;
using Scheduled.Task.Manager.Models.BJ2Miff;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using static Scheduled.Task.Manager.Models.BJ2Miff.ManageBJ;

namespace Scheduled.Task.Manager
{
    class EdiTester
    {



        public EdiTester()
        {

            var filePaths = Directory.GetFiles(@"C:\Users\alouadh\Desktop\AdelphaTech\Mitco\BluJay\FTPFILES\", "*.edi").ToList();
            EdiMapping ediMapping = EdiMapping.Load(@"C:\Users\alouadh\Desktop\AdelphaTech\Mitco\BluJay\FTPFILES\" + "204MAP.xml");

            string txtRslt = "";

            foreach (var item in filePaths)
            {
                EdiDocument ediDocument = EdiDocument.Load(item);
                var transactionSets = ediDocument.TransactionSets;
                foreach (var transaction in transactionSets)
                {
                    XDocument xml = ediMapping.Map(transaction.Segments);
                    var serializer = new XmlSerializer(typeof(TemplateXML204.mapping));
                    TemplateXML204.mapping rslt = (TemplateXML204.mapping)serializer.Deserialize(xml.CreateReader());

                    txtRslt += "SELECT '" + rslt.B2.B204 + "' shipmentID,'" + rslt.L11[0].L1101 + "' TenderID UNION " + Environment.NewLine;


                }



            }


            using (StreamWriter writer = new StreamWriter(@"C:\Users\alouadh\Desktop\AdelphaTech\Mitco\BluJay\FTPFILES\TenderIDs.txt"))
            {
                writer.Write(txtRslt);
            }





        }
    }
}
