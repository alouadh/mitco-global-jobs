﻿using ExcelDataReader;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Scheduled.Task.Manager.Models.studio_pull
{
    class StudioPull
    {

        public static bool sendDatatoDB(DataTable dt, string cnx)
        {
            try
            {
                using (var bulkCopy = new SqlBulkCopy(cnx, SqlBulkCopyOptions.KeepIdentity))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }
                    bulkCopy.BulkCopyTimeout = 15000;
                    bulkCopy.DestinationTableName = "mitcoltd.dbo.lulus_studio_pull";
                    bulkCopy.WriteToServer(dt);
                }
                return true;
            }
            catch (Exception e)
            {
                Log.Error("sendDatatoDB - {@E}", e);
                return false;
            }

        }



        public static DataTable GetExcdata(string filePath)
        {
            try
            {
                DataTable dt = new DataTable();
                var fstr = File.Open(filePath, FileMode.Open, FileAccess.Read);
                var reader = ExcelReaderFactory.CreateCsvReader(fstr, new ExcelReaderConfiguration()
                {
                    FallbackEncoding = Encoding.GetEncoding(1252),
                    AutodetectSeparators = new char[] { ',', ';', '\t', '|', '#' },
                    LeaveOpen = false,
                    AnalyzeInitialCsvRows = 0
                });
                dt = reader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                }).Tables[0];

                DataColumn FileName = new DataColumn("FileName", typeof(String))
                {
                    DefaultValue = Path.GetFileName(filePath)
                };
                dt.Columns.Add(FileName);

                DataColumn ImportDate = new DataColumn("ImportDate", typeof(String))
                {
                    DefaultValue = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")
                };
                dt.Columns.Add(ImportDate);

                DataColumn ImportStatus = new DataColumn("ImportStatus", typeof(int))
                {
                    DefaultValue = 0
                };
                dt.Columns.Add(ImportStatus);

                return dt;
            }
            catch (Exception e)
            {
                Log.Error("GetCsvdata - File Path == " + filePath + " : {@Exception}", e);
                return null;
            }

        }

        public static object Execute(string commande, SqlConnection cn)
        {
            //commande = (apos(commande))
            DataTable dt = new DataTable();
            SqlCommand cd = new SqlCommand(commande, cn);
            cd.CommandTimeout = 0;
            SqlDataAdapter d = new SqlDataAdapter(cd);
            DataSet dset = new DataSet();
            d.Fill(dset, "table");
            try
            {
                dt = dset.Tables["table"];
                return dt;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int CheckFiles()
        {

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            string studio_folder = ConfigurationManager.AppSettings["studio_folder"];
            string studio_file = ConfigurationManager.AppSettings["studio_file"];
            string studio_cnx = ConfigurationManager.AppSettings["studio_cnx"];



            Log.Logger = new LoggerConfiguration().WriteTo.File("logs/studiopull/log-.txt", rollingInterval: RollingInterval.Day).CreateLogger();
            string[] StudioFiles = Directory.GetFiles(studio_folder, studio_file);
            string cnx = studio_cnx;
            SqlConnection cn = new SqlConnection(cnx);

            try
            {
                foreach (var file in StudioFiles)
                {
                    try
                    {
                        bool pass = false;
                        DataTable dt = new DataTable();

                        using (var stream = File.Open(file, FileMode.Open, FileAccess.Read))
                        {
                            using (var reader = ExcelReaderFactory.CreateReader(stream))
                            {
                                dt = reader.AsDataSet(new ExcelDataSetConfiguration()
                                {
                                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                                    {
                                        UseHeaderRow = true
                                    }
                                }).Tables[0];

                                dt.Columns[0].ColumnName = "PONumber";
                                dt.Columns[1].ColumnName = "Vendor";
                                dt.Columns[2].ColumnName = "Name";
                                dt.Columns[3].ColumnName = "Color";
                                dt.Columns[4].ColumnName = "Size";
                                dt.Columns[5].ColumnName = "Qty";
                                dt.Columns[6].ColumnName = "ProductID";
                                dt.Columns[7].ColumnName = "SKU ID";
                                dt.Columns[8].ColumnName = "Division";

                                DataColumn created = new DataColumn("created", typeof(DateTime))
                                {
                                    DefaultValue = DateTime.Now
                                };
                                dt.Columns.Add(created);

                                DataColumn location = new DataColumn("location", typeof(String))
                                {
                                    DefaultValue = "Vc"
                                };
                                dt.Columns.Add(location);

                                DataColumn origin_qty = new DataColumn("origin_qty", typeof(String))
                                {
                                    DefaultValue = "-"
                                };
                                dt.Columns.Add(origin_qty);
                            }
                        }

                        pass = sendDatatoDB(dt, cnx);
                        if (pass)
                        {
                            if (!Directory.Exists(studio_folder + "Processed"))
                            {
                                Directory.CreateDirectory(studio_folder + "Processed");
                            }

                            File.Move(file, studio_folder + "Processed/" + Path.GetFileName(file));
                            string sql = @"update lulus_studio_pull
set recvitemid = ri.id
FROM lulus_studio_pull s, recvitems ri
WHERE rtrim(RI.pono) = s.PONumber 
AND rtrim(RI.sku) = s.[SKU ID] 
AND rtrim(RI.size) = s.Size  
and rtrim(RI.location) = 'LUCH003' 
and s.origin_qty='-'
and s.recvitemid is null";
                            Execute(sql, cn);
                        }
                        else
                        {
                            if (!Directory.Exists(studio_folder + "Not-Processed"))
                            {
                                Directory.CreateDirectory(studio_folder + "Not-Processed");
                            }
                            File.Move(file, studio_folder + "Not-Processed/" + Path.GetFileName(file));
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error("CheckFiles() - ERROR : " + ex.Message);
                        if (!Directory.Exists(studio_folder + "Not-Processed"))
                        {
                            Directory.CreateDirectory(studio_folder + "Not-Processed");
                        }
                        File.Move(file, studio_folder + "Not-Processed/" + Path.GetFileName(file));
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                Log.Error("CheckFiles() - ERROR : " + ex.Message);
                return 0;
            }





        }


    }
}
