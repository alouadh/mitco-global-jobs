﻿using Serilog;
using Serilog.Sinks.LiteDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Scheduled.Task.Manager.Models.MiffStatus
{
    class MiffStatus
    {


        public static object Execute(string commande, SqlConnection cn)
        {
            //commande = (apos(commande))
            DataTable dt = new DataTable();
            SqlCommand cd = new SqlCommand(commande, cn);
            cd.CommandTimeout = 0;
            SqlDataAdapter d = new SqlDataAdapter(cd);
            DataSet dset = new DataSet();
            d.Fill(dset, "table");
            try
            {
                dt = dset.Tables["table"];
                return dt;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }


        public int UpdateMiffStatusGlobal()
        {

            // var LUlog = new LoggerConfiguration().WriteTo.LiteDB("logs/MIFF/MIFFlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var LUlog = new LoggerConfiguration().WriteTo.File("logs/MIFFSTATUS/MIFFSTATUSlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();

            string studio_cnx = ConfigurationManager.AppSettings["studio_cnx"];

            SqlConnection cn = new SqlConnection(studio_cnx);

            try
            {
                //-- STEP 1 : Set OM Order DATE in RECEIVES 
                string sql = @"exec set_orders_management_status";
                Execute(sql, cn);

                return 1;
            }
            catch (Exception ex)
            {
                LUlog.Error("UpdateMiffStatusGlobal : " + ex.Message);
                return 0;
            }

        }

    }
}
