﻿using Microsoft.AspNetCore.Authentication.OAuth.Claims;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Scheduled.Task.Manager.Models.Miff_Pickup_Locations
{



    public class Locations
    {
        public string TRANS_RESULT_KEY { get; set; } = "";
        public string Exception { get; set; } = "";
        public string count { get; set; } = "";
        public List<Location> list { get; set; } = null;
    }

    public class Location
    {
        public string vslCd { get; set; } = "";
        public string etaFlg { get; set; } = "";
        public string vpsEtbDt { get; set; } = "";
        public string mrn { get; set; } = "";
        public string cct { get; set; } = "";
        public string vslSlanCd { get; set; } = "";
        public string etbFlg { get; set; } = "";
        public string vpsEtaDt { get; set; } = "";
        public string vpsPortCd { get; set; } = "";
        public string vslEngNm { get; set; } = "";
        public string vslDlayRsnCd { get; set; } = "";
        public string vslDlayRsnDesc { get; set; } = "";
        public string vslSvcTpCd { get; set; } = "";
        public string vpsEtdDt { get; set; } = "";
        public string vslEtc { get; set; } = "";
        public string discCd { get; set; } = "";
        public string skdVoyNo { get; set; } = "";
        public string skdDirCd { get; set; } = "";
        public string vvd { get; set; } = "";
        public string discLocnm { get; set; } = "";
        public string dct { get; set; } = "";
        public string ydNm { get; set; } = "";
        public string ydEml { get; set; } = "";
        public string etdFlg { get; set; } = "";
        public string overtm { get; set; } = "";
        public string tmlNmMskFlg { get; set; } = "";
    }



    class PickupLocations
    {


        private SGBD sg = new SGBD(ConfigurationManager.AppSettings["meijerdb"]);


        public int getLocations()
        {
            var LUlog = new LoggerConfiguration().WriteTo.File("logs/ETA/ETAlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();
            try
            {
                string dtb = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-01");
                string dte = DateTime.Now.AddMonths(2).ToString("yyyy-MM-01");

                Locations USTIW = new Locations();
                Locations USSEA = new Locations();

                USTIW = locs("USTIW", dtb, dte);
                USSEA = locs("USSEA", dtb, dte);

                string sql = @"INSERT INTO vessel_location_eta_tmp(vslCd,etaFlg,vpsEtbDt,mrn,cct,vslSlanCd,etbFlg,
vpsEtaDt,vpsPortCd,vslEngNm,vslDlayRsnCd,vslDlayRsnDesc,vslSvcTpCd,vpsEtdDt,
vslEtc,discCd,skdVoyNo,skdDirCd,vvd,discLocnm,dct,ydNm,ydEml,etdFlg,overtm,
tmlNmMskFlg,updated) VALUES ('@@vslCd@@','@@etaFlg@@','@@vpsEtbDt@@','@@mrn@@','@@cct@@',
'@@vslSlanCd@@','@@etbFlg@@','@@vpsEtaDt@@','@@vpsPortCd@@','@@vslEngNm@@',
'@@vslDlayRsnCd@@','@@vslDlayRsnDesc@@','@@vslSvcTpCd@@','@@vpsEtdDt@@',
'@@vslEtc@@','@@discCd@@','@@skdVoyNo@@','@@skdDirCd@@','@@vvd@@',
'@@discLocnm@@','@@dct@@','@@ydNm@@','@@ydEml@@','@@etdFlg@@','@@overtm@@',
'@@tmlNmMskFlg@@',GETDATE())";


                sg.Execute("Delete from vessel_location_eta_tmp");

                if (USTIW != null)
                {
                    foreach (var item in USTIW.list)
                    {
                        sg.Execute(
                            sql.Replace("@@vslCd@@", item.vslCd)
                                .Replace("@@etaFlg@@", item.etaFlg)
                                .Replace("@@vpsEtbDt@@", item.vpsEtbDt)
                                .Replace("@@mrn@@", item.mrn)
                                .Replace("@@cct@@", item.cct)
                                .Replace("@@vslSlanCd@@", item.vslSlanCd)
                                .Replace("@@etbFlg@@", item.etbFlg)
                                .Replace("@@vpsEtaDt@@", item.vpsEtaDt)
                                .Replace("@@vpsPortCd@@", item.vpsPortCd)
                                .Replace("@@vslEngNm@@", item.vslEngNm)
                                .Replace("@@vslDlayRsnCd@@", item.vslDlayRsnCd)
                                .Replace("@@vslDlayRsnDesc@@", item.vslDlayRsnDesc)
                                .Replace("@@vslSvcTpCd@@", item.vslSvcTpCd)
                                .Replace("@@vpsEtdDt@@", item.vpsEtdDt)
                                .Replace("@@vslEtc@@", item.vslEtc)
                                .Replace("@@discCd@@", item.discCd)
                                .Replace("@@skdVoyNo@@", item.skdVoyNo)
                                .Replace("@@skdDirCd@@", item.skdDirCd)
                                .Replace("@@vvd@@", item.vvd)
                                .Replace("@@discLocnm@@", item.discLocnm)
                                .Replace("@@dct@@", item.dct)
                                .Replace("@@ydNm@@", item.ydNm)
                                .Replace("@@ydEml@@", item.ydEml)
                                .Replace("@@etdFlg@@", item.etdFlg)
                                .Replace("@@overtm@@", item.overtm)
                                .Replace("@@tmlNmMskFlg@@", item.tmlNmMskFlg)
                            );
                    }
                }
                if (USSEA != null)
                {
                    foreach (var item in USSEA.list)
                    {
                        sg.Execute(
                            sql.Replace("@@vslCd@@", item.vslCd)
                                .Replace("@@etaFlg@@", item.etaFlg)
                                .Replace("@@vpsEtbDt@@", item.vpsEtbDt)
                                .Replace("@@mrn@@", item.mrn)
                                .Replace("@@cct@@", item.cct)
                                .Replace("@@vslSlanCd@@", item.vslSlanCd)
                                .Replace("@@etbFlg@@", item.etbFlg)
                                .Replace("@@vpsEtaDt@@", item.vpsEtaDt)
                                .Replace("@@vpsPortCd@@", item.vpsPortCd)
                                .Replace("@@vslEngNm@@", item.vslEngNm)
                                .Replace("@@vslDlayRsnCd@@", item.vslDlayRsnCd)
                                .Replace("@@vslDlayRsnDesc@@", item.vslDlayRsnDesc)
                                .Replace("@@vslSvcTpCd@@", item.vslSvcTpCd)
                                .Replace("@@vpsEtdDt@@", item.vpsEtdDt)
                                .Replace("@@vslEtc@@", item.vslEtc)
                                .Replace("@@discCd@@", item.discCd)
                                .Replace("@@skdVoyNo@@", item.skdVoyNo)
                                .Replace("@@skdDirCd@@", item.skdDirCd)
                                .Replace("@@vvd@@", item.vvd)
                                .Replace("@@discLocnm@@", item.discLocnm)
                                .Replace("@@dct@@", item.dct)
                                .Replace("@@ydNm@@", item.ydNm)
                                .Replace("@@ydEml@@", item.ydEml)
                                .Replace("@@etdFlg@@", item.etdFlg)
                                .Replace("@@overtm@@", item.overtm)
                                .Replace("@@tmlNmMskFlg@@", item.tmlNmMskFlg)
                            );
                    }
                }


                // Create TEMP VESSELS DATA

                string sqli = @"INSERT INTO vessel_location_eta([vslCd],[etaFlg],[vpsEtbDt],[mrn],[cct],
[vslSlanCd],[etbFlg],[vpsEtaDt],[vpsPortCd],[vslEngNm],[vslDlayRsnCd],
[vslDlayRsnDesc],[vslSvcTpCd],[vpsEtdDt],[vslEtc],[discCd],[skdVoyNo],[skdDirCd]
,[vvd],[discLocnm],[dct],[ydNm],[ydEml],[etdFlg],[overtm],[tmlNmMskFlg],updated)(select 
vt.[vslCd],vt.[etaFlg],vt.[vpsEtbDt],vt.[mrn],vt.[cct],vt.[vslSlanCd],vt.
[etbFlg],vt.[vpsEtaDt],vt.[vpsPortCd],vt.[vslEngNm],vt.[vslDlayRsnCd],vt.
[vslDlayRsnDesc],vt.[vslSvcTpCd],vt.[vpsEtdDt],vt.[vslEtc],vt.[discCd],vt.
[skdVoyNo],vt.[skdDirCd],vt.[vvd],vt.[discLocnm],vt.[dct],vt.[ydNm],vt.[ydEml],
vt.[etdFlg],vt.[overtm],vt.[tmlNmMskFlg], GETDATE() from vessel_location_eta_tmp vt where vt.vvd 
not in(select vvd from vessel_location_eta where convert(datetime,vpsEtaDt)>=GETDATE()
-90))";
                sg.Execute(sqli);



                // Update New Vessels DATA
                string sqlu = @"update vessel_location_eta
set vpsEtaDt = vt.vpsEtaDt
,   discLocnm = vt.discLocnm
,   ydNm = vt.ydNm
,   discCd = vt.discCd
,   etdFlg = vt.etdFlg
,   updated = GETDATE()
from 
vessel_location_eta v, vessel_location_eta_tmp vt
where v.vvd = vt.vvd
and (v.vpsEtaDt <> vt.vpsEtaDt or v.discLocnm <> vt.discLocnm)
and convert(datetime,v.vpsEtaDt)>=GETDATE()-90";

                sg.Execute(sqlu);


                // Add New Addresses
                string sqla = @"insert into address (addrid,company,address)
(
	select distinct 
	discCd addrid,discLocnm company, vpsPortCd + ' - ' + discLocnm address 
	from vessel_location_eta ve
	where (select count(*) from address a where rtrim(a.addrid) = ve.discCd) = 0
)";


                sg.Execute(sqla);

                // Update Receive Pickup Locations and ETA
                string sqlv = @"update Vessel
set ETA = ve.vpsEtaDt
,   pickupLocation = ve.discCd
,   lastETA = ETA
from Vessel v, vessel_location_eta ve
where v.ETA >=GETDATE()
and rtrim(v.VoyageName) like '%'+ve.vslEngNm+'%'
and rtrim(v.VoyageName) like '%'+convert(varchar,convert(int,ve.skdVoyNo))+'%'
and format(ve.updated,'yyyy-MM-dd') = format(GETDATE(),'yyyy-MM-dd')";

                sg.Execute(sqlv);

                // Update Receive Pickup Locations and ETA
                string sqlo = @"update orders
set pickuplocid = v.pickupLocation
, vesseleta = v.ETA
from orders o, Vessel v
where o.voyageid = v.Voyageid
and o.pickuplocid is null
and v.pickupLocation is not null ";

                sg.Execute(sqlo);




                // Update Receive Pickup Locations
                string sqlr = @"update receive
set pickuplocid = o.pickuplocid
from receive r, orders o
where r.orderid = o.orderid
and o.pickuplocid is not null
and o.voyageid in (
select 
	v.Voyageid 
from Vessel v
where v.pickupLocation is not null
)
and isnull(r.pickuplocid,'')  = '' ";

                sg.Execute(sqlr);


                return 1;
            }
            catch (Exception ex)
            {
                LUlog.Error("ETA : " + ex.Message);
                return 0;
            }

            




        }




        public Locations locs(string port, string dtb, string dte)
        {
            try
            {
                var client = new RestClient("https://ecomm.one-line.com/ecom/CUP_HOM_3006GS.do?f_cmd=121&mod_flg=I&port_cd=" + port + "&frm_dt=" + dtb + "&to_dt=" + dte);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);

                // Console.WriteLine(response.Content);

                Locations locat = new Locations();
                locat = JsonConvert.DeserializeObject<Locations>(response.Content);

                return locat;
            }
            catch (Exception ex)
            {
                return null;
            }

        }


    }
}
