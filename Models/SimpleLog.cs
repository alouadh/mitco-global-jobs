﻿using Serilog;
using Serilog.Sinks.LiteDB;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduled.Task.Manager.Models
{
    class SimpleLog
    {

        public void LogITPls()
        {
            var log = new LoggerConfiguration().WriteTo.LiteDB("logs/logs.db", logCollectionName : "applog", rollingFilePeriod: RollingPeriod.Quarterly).CreateLogger();
            log.Information("AT-Scheduled.Task.Manager#Service is here");

            try
            {
                throw new DivideByZeroException();
            }
            catch (DivideByZeroException ex)
            {
                log.Error("{@Ex}", ex);
            }


        }


    }
}
