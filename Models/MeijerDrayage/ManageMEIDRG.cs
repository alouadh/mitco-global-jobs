﻿using Newtonsoft.Json;
using RestSharp;
using Serilog;
using Serilog.Sinks.LiteDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Scheduled.Task.Manager.Models.MeijerDrayage
{
    class ManageMEIDRG
    {



        public class ImportItemI
        {
            [JsonProperty("Deconsol SCAC")]
            public string DeconsolSCAC { get; set; }
            [JsonProperty("Mater Bill of Lading")]
            public string MaterBillofLading { get; set; }
            [JsonProperty("Ocean Container Number")]
            public string OceanContainerNumber { get; set; }
            [JsonProperty("Meijer Import Load Number")]
            public int MeijerImportLoadNumber { get; set; }
            [JsonProperty("Transportation Method Category")]
            public string TransportationMethodCategory { get; set; }
            [JsonProperty("Ocean Carrier SCAC")]
            public string OceanCarrierSCAC { get; set; }
            [JsonProperty("Ocean Vessel Name")]
            public string OceanVesselName { get; set; }
            [JsonProperty("Ocean Voyage ID")]
            public string OceanVoyageID { get; set; }
            [JsonProperty("Sail Date")]
            public DateTime SailDate { get; set; }
            [JsonProperty("Shipping Terms")]
            public string ShippingTerms { get; set; }
            [JsonProperty("Container Arrival Date")]
            public DateTime ContainerArrivalDate { get; set; }
            [JsonProperty("Container Receipt Date")]
            public DateTime ContainerReceiptDate { get; set; }
            [JsonProperty("Freight Forwarder")]
            public string FreightForwarder { get; set; }
            [JsonProperty("Port of Loading Name")]
            public string PortofLoadingName { get; set; }
            [JsonProperty("Port of Discharge Name")]
            public string PortofDischargeName { get; set; }
            [JsonProperty("Container Destination Name")]
            public string ContainerDestinationName { get; set; }
            [JsonProperty("Customs Clearance Flag")]
            public string CustomsClearanceFlag { get; set; }
            [JsonProperty("Customs Clearance Timestamp")]
            public DateTime CustomsClearanceTimestamp { get; set; }
            [JsonProperty("Country ISO")]
            public string CountryISO { get; set; }
            [JsonProperty("Port of Discharge Country ID")]
            public string PortofDischargeCountryID { get; set; }
            [JsonProperty("Container Ship To Name")]
            public string ContainerShipToName { get; set; }
            [JsonProperty("Container Seal ID")]
            public string ContainerSealID { get; set; }
            [JsonProperty("Container Size")]
            public string ContainerSize { get; set; }
            [JsonProperty("Purchase Order Number")]
            public int PurchaseOrderNumber { get; set; }
            [JsonProperty("Meijer DF Number")]
            public int MeijerDFNumber { get; set; }
            [JsonProperty("Vendor Name")]
            public string VendorName { get; set; }
            [JsonProperty("Buyer ID")]
            public int BuyerID { get; set; }
            [JsonProperty("Promo Date")]
            public DateTime PromoDate { get; set; }
            [JsonProperty("UPC Number")]
            public string UPCNumber { get; set; }
            [JsonProperty("UPC Description")]
            public string UPCDescription { get; set; }
            [JsonProperty("Load Sequence ID")]
            public string LoadSequenceID { get; set; }
            [JsonProperty("Product ID")]
            public string ProductID { get; set; }
            [JsonProperty("Case Pack ID")]
            public string CasePackID { get; set; }
            [JsonProperty("Item Pack Size")]
            public int ItemPackSize { get; set; }
            [JsonProperty("Total Item Carton QTY")]
            public int TotalItemCartonQTY { get; set; }
            [JsonProperty("Total Item CBM")]
            public string TotalItemCBM { get; set; }
            [JsonProperty("Total Item Weight")]
            public string TotalItemWeight { get; set; }
            [JsonProperty("Department")]
            public string Department { get; set; }
            // [JsonProperty("RW_UDT_TS")]
            [JsonProperty("PO Expected Receipt Date")]
            public DateTime RW_UDT_TS { get; set; }
        }



        public int updateMeijerTables()
        {

            // var MEIlog = new LoggerConfiguration().WriteTo.LiteDB("logs/MEIDRG/MEIDRGlogs.db", logCollectionName: "applog", rollingFilePeriod: RollingPeriod.Monthly).CreateLogger();
            var MEIlog = new LoggerConfiguration().WriteTo.File("logs/MEIDRG/MEIDRGlogs.db", rollingInterval: RollingInterval.Day).CreateLogger();


            // ServicePointManager.ServerCertificateValidationCallback = new TrustAllCertificatePolicy();
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;

            string api_partner_end_point = ConfigurationManager.AppSettings["meijer_import_end_point"];
            string meijer_import_end_point_key = ConfigurationManager.AppSettings["meijer_import_end_point_key"];
            string IN_SCAC_ID = ConfigurationManager.AppSettings["IN_SCAC_ID"];
            string IN_CLHR = ConfigurationManager.AppSettings["IN_CLHR"];
            SGBD sg = new SGBD(ConfigurationManager.AppSettings["meijerdb"]);
            try
            {
                var client = new RestClient(api_partner_end_point + "IN_SCAC_ID=" + IN_SCAC_ID + "&IN_CLHR=" + IN_CLHR + "&key=" + meijer_import_end_point_key);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                List<ImportItemI> result = JsonConvert.DeserializeObject<List<ImportItemI>>(response.Content);

                if (result.Count() > 0)
                {
                    foreach (ImportItemI item in result)
                    {
                        try
                        {
                            string sql = "";
                            sql += "INSERT INTO [dbo].[api_import_meijer] ";
                            sql += "  ([import_date] ";
                            sql += "  ,[DeconsolSCAC] ";
                            sql += "  ,[MaterBillofLading] ";
                            sql += "  ,[OceanContainerNumber] ";
                            sql += "  ,[MeijerImportLoadNumber] ";
                            sql += "  ,[TransportationMethodCategory] ";
                            sql += "  ,[OceanCarrierSCAC] ";
                            sql += "  ,[OceanVesselName] ";
                            sql += "  ,[OceanVoyageID] ";
                            sql += "  ,[SailDate] ";
                            sql += "  ,[ShippingTerms] ";
                            sql += "  ,[ContainerArrivalDate] ";
                            sql += "  ,[ContainerReceiptDate] ";
                            sql += "  ,[FreightForwarder] ";
                            sql += "  ,[PortofLoadingName] ";
                            sql += "  ,[PortofDischargeName] ";
                            sql += "  ,[ContainerDestinationName] ";
                            sql += "  ,[CustomsClearanceFlag] ";
                            sql += "  ,[CustomsClearanceTimestamp] ";
                            sql += "  ,[CountryISO] ";
                            sql += "  ,[PortofDischargeCountryID] ";
                            sql += "  ,[ContainerShipToName] ";
                            sql += "  ,[ContainerSealID] ";
                            sql += "  ,[ContainerSize] ";
                            sql += "  ,[PurchaseOrderNumber] ";
                            sql += "  ,[MeijerDFNumber] ";
                            sql += "  ,[VendorName] ";
                            sql += "  ,[BuyerID] ";
                            sql += "  ,[PromoDate] ";
                            sql += "  ,[UPCNumber] ";
                            sql += "  ,[UPCDescription] ";
                            sql += "  ,[LoadSequenceID] ";
                            sql += "  ,[ProductID] ";
                            sql += "  ,[CasePackID] ";
                            sql += "  ,[ItemPackSize] ";
                            sql += "  ,[TotalItemCartonQTY] ";
                            sql += "  ,[TotalItemCBM] ";
                            sql += "  ,[TotalItemWeight] ";
                            sql += "  ,[Department] ";
                            sql += "  ,[RW_UDT_TS] ";
                            sql += "  ,[import_status]) ";
                            sql += "     VALUES ";
                            sql += " (getdate() ";
                            sql += " ,'" + item.DeconsolSCAC.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.MaterBillofLading.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.OceanContainerNumber.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.MeijerImportLoadNumber.ToString() + "' ";
                            sql += " ,'" + item.TransportationMethodCategory.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.OceanCarrierSCAC.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.OceanVesselName.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.OceanVoyageID.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.SailDate.ToString("yyyy-MM-dd") + "' ";
                            sql += " ,'" + item.ShippingTerms.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.ContainerArrivalDate.ToString("yyyy-MM-dd") + "' ";
                            sql += " ,'" + item.ContainerReceiptDate.ToString("yyyy-MM-dd") + "' ";
                            sql += " ,'" + item.FreightForwarder.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.PortofLoadingName.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.PortofDischargeName.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.ContainerDestinationName.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.CustomsClearanceFlag.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.CustomsClearanceTimestamp.ToString("yyyy-MM-dd") + "' ";
                            sql += " ,'" + item.CountryISO.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.PortofDischargeCountryID.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.ContainerShipToName.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.ContainerSealID.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.ContainerSize.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.PurchaseOrderNumber.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.MeijerDFNumber.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.VendorName.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.BuyerID.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.PromoDate.ToString("yyyy-MM-dd") + "' ";
                            sql += " ,'" + item.UPCNumber.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.UPCDescription.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.LoadSequenceID.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.ProductID.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.CasePackID.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.ItemPackSize.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.TotalItemCartonQTY.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.TotalItemCBM.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.TotalItemWeight.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.Department.ToString().Replace("'", "''") + "' ";
                            sql += " ,'" + item.RW_UDT_TS.ToString("yyyy-MM-dd") + "' ";
                            sql += " ,'0' ";
                            sql += " ) ";

                            sg.Execute(sql);

                        }
                        catch (Exception ex)
                        {
                            MEIlog.Error("Drayage Insert - ERROR : " + ex.Message);
                        }


                    }





                }


                try
                {
                    sg.Execute("EXECUTE api_setup_meijer_data");
                }
                catch (Exception ex1)
                {
                    MEIlog.Error("Meijer SETUP import - ERROR " + ex1.Message);
                }
                return 1;
            }
            catch (Exception ex)
            {
                MEIlog.Error("UpdateMeijerTables Drayage - ERROR " + ex.Message);
                return 0;
            }


        }

    }
}
