﻿using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace Scheduled.Task.Manager.Models.BJ2Miff
{
    public class FtpTools
    {


        public string Host, Username, Password, FromFolder, ToFolder;
        SftpClient client;
        public FtpTools(string Host, string Username, string Password, string FromFolder, string ToFolder)
        {
            this.Host = Host;
            this.Username = Username;
            this.Password = Password;
            this.FromFolder = FromFolder;
            this.ToFolder = ToFolder;
            this.client = new SftpClient(Host, Username, Password);
        }


        public void DownloadFile(SftpFile file)
        {
            var sftp = new SftpClient(this.Host, this.Username, this.Password);
            sftp.Connect();
            using (Stream fileStream = File.OpenWrite(Path.Combine(this.ToFolder, file.Name)))
            {
                sftp.DownloadFile(file.FullName, fileStream);
            }
            sftp.Disconnect();
        }


        public bool UploadFile(string fileName)
        {
            bool isProcessed = true;
            using (var sftp = new SftpClient(this.Host, this.Username, this.Password))
            {
                try
                {
                    sftp.Connect();
                    

                    using (var uplfileStream = File.OpenRead(fileName))
                    {
                        sftp.UploadFile(uplfileStream, this.FromFolder + Path.GetFileName(fileName), true);
                        isProcessed = true;
                    }
                }
                catch (Exception ex)
                {
                    isProcessed = false;                    
                }
                finally
                {
                    sftp.Disconnect();
                    sftp.Dispose();
                }
            }
            return isProcessed;
        }



        // Get files List
        public IEnumerable<SftpFile> GetList()
        {
            using (var sftp = this.client)
            {
                sftp.Connect();
                var toReturn = sftp.ListDirectory(this.FromFolder).ToList();
                sftp.Disconnect();
                return toReturn;
            }
        }


        public List<SftpFile> GetRemoteFilesOrFoldersList(int type)
        {
            List<SftpFile> fList = new List<SftpFile>();
            using (SftpClient client = this.client)
            {
                client.KeepAliveInterval = TimeSpan.FromSeconds(60);
                client.ConnectionInfo.Timeout = TimeSpan.FromMinutes(180);
                client.OperationTimeout = TimeSpan.FromMinutes(180);
                client.Connect();
                var fileList = client.ListDirectory(this.FromFolder);
                foreach (var f in fileList)
                {
                    if (f.Name != "." && f.Name != "..")
                    {
                        if (type == 1)
                        {// Get folders
                            if (f.IsDirectory)
                            {
                                fList.Add(f);
                            }
                        }
                        else
                        {
                            if (f.IsRegularFile)
                            {
                                fList.Add(f);
                            }
                        }
                    }
                }
            }
            return fList;
        }




    }
}